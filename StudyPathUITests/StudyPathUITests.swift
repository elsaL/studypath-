import XCTest

class StudyPathUITests: XCTestCase {
    var app: XCUIApplication!

    override func setUp() {
        
        super.setUp()
        continueAfterFailure = false
        app = XCUIApplication()
        app.launch()

        app.buttons["Join Free"].tap()
        let nameTextField = app.textFields["Username"]
        nameTextField.tap()
        nameTextField.typeText("testuserA")
        
        let emailTextField = app.textFields["Email"]
        emailTextField.tap()
        emailTextField.typeText("aaa@aaa.fi")
        
        let pwTextField = app.secureTextFields["Password"]
        pwTextField.tap()
        pwTextField.typeText("testuserA")
        
        let rppwTextField = app.secureTextFields["Confirm Password"]
        rppwTextField.tap()
        rppwTextField.typeText("testuserA")
        
        app.buttons["Register"].tap()
    }

    override func tearDown() {
    }

    func testExample() {
        
    }
    
    func testLoginButtonDisplays() {
        app.launch()
        XCTAssertTrue(app.buttons["Login"].exists)
    }
    
    
    func testJoinFreeButtonShowsRegisterUi() {
        app.launch()
        app.buttons["Join Free"].tap()
        XCTAssertTrue(app.buttons["Login"].exists)
        XCTAssertTrue(app.buttons["Register"].exists)
        XCTAssertFalse(app.buttons["Join Free"].exists)
    }
    
    
    func testRegisterWithEmptyUsernameAlertMessageDisplays() {
        app.launch()
        app.buttons["Join Free"].tap()
        app.buttons["Register"].tap()
        
        XCTAssertTrue(app.alerts.element.staticTexts[
            "Username needs to be more than 2 characters"].exists)
    }
    
    func testLoginWithEmptyUsernameAlertMessageDisplays() {
        app.launch()
        app.buttons["Login"].tap()
        
        XCTAssertTrue(app.alerts.element.staticTexts[
            "Username does not exist"].exists)
    }
    
    
    func testLoginWithWrongPasswordAlertMessageDisplays() {
        app.launch()
        
        let nameTextField = app.textFields["Username"]
         nameTextField.tap()
        nameTextField.typeText("testuserA")
        
        let pwTextField = app.secureTextFields["Password"]
        pwTextField.tap()
        pwTextField.typeText("aaa") //wrong password
       
        app.buttons["Login"].tap()
        
        XCTAssertTrue(app.alerts.element.staticTexts[
            "password is not correct"].exists)
    }
    
    func testNewUserSuccessLoginGoToOnboarding() {
        app.launch()
        
        let nameTextField = app.textFields["Username"]
        nameTextField.tap()
        nameTextField.typeText("testuserA")
        
        let pwTextField = app.secureTextFields["Password"]
        pwTextField.tap()
        pwTextField.typeText("testuserA")
        
        app.buttons["Login"].tap()
        
        XCTAssertTrue(app.buttons["Start"].exists)
        
        app.buttons["Start"].tap()
        
        XCTAssertTrue(app.buttons["Next"].exists)
    }

}
