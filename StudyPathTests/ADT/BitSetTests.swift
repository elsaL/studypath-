//
//  BitSetTests.swift
//  StudyPathTests
//
//  Created by iosdev on 24/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import XCTest
@testable import StudyPath

class BitSetTests: XCTestCase {
    let emptyBitset = BitSet<Int32>.empty
    let bitset2 = (try? BitSet<Int32>.singleton(2)) ?? BitSet.empty
    let bitset5 = (try? BitSet<Int32>.singleton(5)) ?? BitSet.empty
    let bitset14 = (try? BitSet<Int32>.singleton(14)) ?? BitSet.empty
    let bitsetPrimes = (try? BitSet<Int32>.fromArray([2,3,5,7,11,13])) ?? BitSet.empty
    let bitset40Err = try? BitSet<Int32>.singleton(40)
    let bitsetM2Err = try? BitSet<Int32>.singleton(-2)
    
    func testUnionIntersection() {
        let bitsetA = bitset2 + bitset5
        let bitsetB = bitset5 + bitset14
        
        let union = bitsetA + bitsetB
        let intersection = bitsetA.intersection(bitsetB)
        
        XCTAssertEqual(bitsetA.count, 2)
        XCTAssertEqual(bitsetB.count, 2)
        XCTAssertEqual(union.count, 3)
        XCTAssertEqual(intersection.count, 1)
        
        XCTAssert(union.contains(2))
        XCTAssert(union.contains(5))
        XCTAssert(union.contains(14))
        
        XCTAssert(intersection.contains(5))
    }
    
    func testDifference() {
        let bitsetA = bitset2 + bitset5
        let bitsetB = bitset5 + bitset14
        
        let diff = bitsetA - bitsetB
        
        XCTAssertEqual(diff.count, 1)
        
        XCTAssert(diff.contains(2))
        
    }
    
    func testToIntSet() {
        let bitsetA = bitset2 + bitset5
        let bitsetB = bitset5 + bitset14
        
        let union = bitsetA + bitsetB
        let intersection = bitsetA.intersection(bitsetB)
        
        XCTAssertEqual(bitsetA.toIntSet().count, 2)
        XCTAssertEqual(bitsetB.toIntSet().count, 2)
        XCTAssertEqual(union.toIntSet().count, 3)
        XCTAssertEqual(intersection.toIntSet().count, 1)
        
        XCTAssert(union.toIntSet().contains(2))
        XCTAssert(union.toIntSet().contains(5))
        XCTAssert(union.toIntSet().contains(14))
        
        XCTAssert(intersection.toIntSet().contains(5))
    }
    
    func testFromArray() {
        let bitsetA = bitset2 + bitset5 + bitset14
        
        XCTAssertEqual(bitsetPrimes.count, 6)
        XCTAssert(bitsetPrimes.contains(2))
        XCTAssert(bitsetPrimes.contains(3))
        XCTAssert(bitsetPrimes.contains(5))
        XCTAssert(bitsetPrimes.contains(7))
        XCTAssert(bitsetPrimes.contains(11))
        XCTAssert(bitsetPrimes.contains(13))
        
        
        let union = bitsetA + bitsetPrimes
        let intersection = bitsetA.intersection(bitsetPrimes)
        
        XCTAssertEqual(union.count, 7)
        XCTAssertEqual(intersection.count, 2)
    }
    
    func testOutOfRange() {
        XCTAssert(bitset40Err == nil)
        XCTAssert(bitsetM2Err == nil)
    }

}
