//
//  FunctionalExtensionsTests.swift
//  StudyPathTests
//
//  Created by iosdev on 24/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import XCTest
@testable import StudyPath

class FunctionalExtensionsTests: XCTestCase {

    let arrA = [1,2,3,4,5]
    let arrB = ["a", "b", "c", "d"]
    let optArrA: [Int?] = [1, 2, 3, 4, 5, 6, 7]
    let optArrB: [Int?] = [1, 2, 3, 4, 5, nil, 7]
    
    func testZipWith(){
        let result = arrA.zipWith(arrB, combine: {(num, str) in
            str + String(num)
        })
        
        XCTAssertEqual(result.count, 4)
        XCTAssert(result.contains("a1"))
        XCTAssert(result.contains("b2"))
        XCTAssert(result.contains("c3"))
        XCTAssert(result.contains("d4"))
        
    }

}
