//
//  StudyPathTests.swift
//  StudyPathTests
//
//  Created by iosdev on 28/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import XCTest
@testable import StudyPath

class StudyPathTests: XCTestCase {

    let emptyStudyPath = StudyPath.empty(targetHours: 300)
    
    //NOTE: A clashes with B and maybe clashed with E
    
    let courseA = Course(name: "A",
                        description: "A desc",
                        provider: "",
                        contactTeaching: false,
                        language: ["English"],
                        location: "Helsinki",
                        model: ModelCourse(id: "MA", name: "MA", level: 0, topics: ["A", "B"]),
                        schedule: ReservedSchedule.empty.withReserved(year: 1, weeks: Array(10..<15), weeklySchedule: WeeklySchedule.empty.withSameBookingsEveryWorkingDay(times: [.morning, .afternoon])),
                        tags: ["aa"],
                        hours: 80,
                        imageUrl: nil,
                        url: nil)
    
    let courseB = Course(name: "B",
                         description: "B desc",
                         provider: "",
                         contactTeaching: false,
                         language: ["English"],
                         location: "Helsinki",
                         model: ModelCourse(id: "MA", name: "MB", level: 0, topics: ["B"]),
                         schedule: ReservedSchedule.empty.withReserved(year: 1, weeks: Array(12..<20), weeklySchedule: WeeklySchedule.empty.withSameBookingsEveryWorkingDay(times: [.morning, .afternoon])),
                         tags: ["aa", "bb"],
                         hours: 50,
                         imageUrl: nil,
                         url: nil)

    let courseC = Course(name: "C",
                         description: "C desc",
                         provider: "",
                         contactTeaching: false,
                         language: ["English"],
                         location: "Helsinki",
                         model: ModelCourse(id: "MA", name: "MC", level: 0, topics: ["A"]),
                         schedule: ReservedSchedule.empty.withReserved(year: 1, weeks: Array(20..<25), weeklySchedule: WeeklySchedule.empty.withSameBookingsEveryWorkingDay(times: [.morning, .afternoon])),
                         tags: ["aa", "bb"],
                         hours: 30,
                         imageUrl: nil,
                         url: nil)
    
    let courseD = Course(name: "D",
                         description: "D desc",
                         provider: "",
                         contactTeaching: false,
                         language: ["English"],
                         location: "Helsinki",
                         model: ModelCourse(id: "MA", name: "MD", level: 0, topics: ["A"]),
                         schedule: ReservedSchedule.empty.withReserved(year: 2, weeks: Array(10..<15), weeklySchedule: WeeklySchedule.empty.withSameBookingsEveryWorkingDay(times: [.morning, .afternoon])),
                         tags: ["bb"],
                         hours: 80,
                         imageUrl: nil,
                         url: nil)
    
    let courseE = Course(name: "E",
                         description: "E desc",
                         provider: "",
                         contactTeaching: false,
                         language: ["English"],
                         location: "Helsinki",
                         model: ModelCourse(id: "MA", name: "ME", level: 0, topics: ["A"]),
                         schedule: ReservedSchedule.empty.withReserved(year: 1, weeks: Array(10..<15), weeklySchedule: WeeklySchedule.empty.withSameBookingsEveryWorkingDay(times: [.unknown])),
                         tags: ["aa"],
                         hours: 120,
                         imageUrl: nil,
                         url: nil)
    
    func testEmptyStudyPath() {
        XCTAssert(!emptyStudyPath.isFull)
        XCTAssert(!emptyStudyPath.isCompleted)
        XCTAssert(emptyStudyPath.allCourses.isEmpty)
        XCTAssertEqual(emptyStudyPath.hours, 0)
    }
    
    func testBuildingStudyPath() {
        let studyPath = emptyStudyPath
            .with(course: courseA)
            .with(course: courseB)
            .with(course: courseC)
            .with(course: courseD)
            .with(course: courseE)
        
        XCTAssertEqual(studyPath.allCourses, [courseA, courseB, courseC, courseD, courseE])
        XCTAssert(studyPath.isFull)
        XCTAssert(!studyPath.isCompleted)
        XCTAssert(studyPath.completedCourses.isEmpty)
        XCTAssertEqual(studyPath.hours, courseA.hours + courseB.hours + courseC.hours + courseD.hours + courseE.hours)
    }
    
    func testFull() {
        let studyPathNotFull = emptyStudyPath
            .with(course: courseE)
            .with(course: courseA)
            .with(course: courseD)
        let studyPathFull = studyPathNotFull
            .with(course: courseB)
        let studyPathBeyondFull = studyPathFull
            .with(course: courseE)
        
        XCTAssert(!studyPathNotFull.isFull)
        XCTAssert(studyPathFull.isFull)
        XCTAssert(studyPathBeyondFull.isFull)
        
    }
    
    func testStartedCompleted() throws {
        let studyPath = emptyStudyPath
            .with(course: courseA)
            .with(course: courseB)
            .with(course: courseC)
            .with(course: courseD)
            .with(course: courseE)
        
        let completedStudyPath = try studyPath
            .completedCourse(at: 1)
            .completedCourse(at: 0)
            .completedCourse(course: courseD)
            .completedCourse(course: courseC)
            .completedCourse(at: 4)
        
        let partialStudyPathA = try studyPath
            .completedCourse(at: 0)
            .completedCourse(course: courseB)
            .startedCourse(course: courseC)
            .startedCourse(at: 3)
        
        let partialStudyPathB = try studyPath
            .completedCourse(at: 0)
            .completedCourse(course: courseB)
            .startedCourse(course: courseC)
            .startedCourse(at: 3)
            .startedCourse(course: courseE)
        
        
        XCTAssert(completedStudyPath.isCompleted)
        XCTAssertEqual(completedStudyPath.allCourses, [courseA, courseB, courseC, courseD, courseE])
        
        XCTAssert(!partialStudyPathA.isCompleted)
        XCTAssertEqual(partialStudyPathA.allCourses, [courseA, courseB, courseC, courseD, courseE])
        XCTAssertEqual(partialStudyPathA.currentCourses, [courseC, courseD])
        XCTAssertEqual(partialStudyPathA.completedCourses, [courseA, courseB])
        
        XCTAssert(!partialStudyPathB.isCompleted)
        XCTAssertEqual(partialStudyPathB.allCourses, [courseA, courseB, courseC, courseD, courseE])
        XCTAssertEqual(partialStudyPathB.currentCourses, [courseC, courseD, courseE])
        XCTAssertEqual(partialStudyPathB.completedCourses, [courseA, courseB])
        
    }
    
    func testReplace() throws {
        let studyPath = emptyStudyPath
            .with(course: courseA)
            .with(course: courseB)
            .with(course: courseC)
            .with(course: courseD)
        
        let fixedStudyPath = try studyPath
            .withReplacement(oldCourse: courseB, newCourse: courseE)
        
        XCTAssertEqual(fixedStudyPath.allCourses, [courseA, courseE, courseC, courseD])
        XCTAssertEqual(fixedStudyPath.hours, courseA.hours + courseE.hours + courseC.hours + courseD.hours)
    }
    
    func testRemove() throws {
        let studyPath = emptyStudyPath
            .with(course: courseA)
            .with(course: courseB)
            .with(course: courseC)
            .with(course: courseD)
        
        let fixedStudyPath = try studyPath
            .withRemoval(at: 0)
            .withRemoval(course: courseC)
        
        XCTAssertEqual(fixedStudyPath.allCourses, [courseB, courseD])
        XCTAssertEqual(fixedStudyPath.hours, courseB.hours + courseD.hours)
    }
    
    func testClashes() throws {
        let studyPathA = emptyStudyPath
            .with(course: courseC)
            .with(course: courseA)
        
        let scheduleB = courseB.schedule
        let scheduleE = courseE.schedule
        
        let clashesB = studyPathA.clashes(schedule: scheduleB)
        let clashesE = studyPathA.clashes(schedule: scheduleE)
        
        XCTAssertEqual(clashesB.clashesCount.surely, 1)
        XCTAssertEqual(clashesB.clashesCount.maybe, 0)
        XCTAssertEqual(clashesB.clashes.first?.course, courseA)
        XCTAssertEqual(clashesB.clashes.first?.index, 1)
        
        XCTAssertEqual(clashesE.clashesCount.surely, 0)
        XCTAssertEqual(clashesE.clashesCount.maybe, 1)
        XCTAssertEqual(clashesE.clashes.first?.course, courseA)
        XCTAssertEqual(clashesE.clashes.first?.index, 1)
        
        
        let studyPathB = emptyStudyPath
            .with(course: courseB)
            .with(course: courseE)
        
        let scheduleA = courseA.schedule
        
        let clashesBB = studyPathB.clashes(schedule: scheduleA)
        
        XCTAssertEqual(clashesBB.clashesCount.surely, 1)
        XCTAssertEqual(clashesBB.clashesCount.maybe, 1)
        XCTAssertEqual(clashesBB.clashes.map{$0.course}, [courseB, courseE])
        XCTAssertEqual(clashesBB.clashes.map{$0.index}, [0, 1])
    }

}
