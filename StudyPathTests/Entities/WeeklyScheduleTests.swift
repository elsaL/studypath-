
import XCTest
@testable import StudyPath

class WeeklyScheduleTests: XCTestCase {

    let weeklyEmpty = WeeklySchedule.empty
    let weeklyA = WeeklySchedule
        .empty
        .withBookingOn(day: .tuesday, time: .morning)
        .withBookingOn(day: .wednesday, time: .morning)
        .withBookingOn(day: .wednesday, time: .afternoon)
        .withBookingOn(day: .thursday, time: .morning)
    
    let weeklyB = WeeklySchedule
        .empty
        .withBookingOn(day: .tuesday, time: .morning)
        .withBookingsOn(day: .wednesday, times: [.morning, .afternoon])
        .withBookingOn(day: .thursday, time: .morning)
    
    let weeklyC = WeeklySchedule
        .empty
        .withSameBookingsOn(days: [.monday, .friday], times: [.morning, .evening])
    
    let weeklyD = WeeklySchedule
        .empty
        .withSameBookingsEveryWorkingDay(times: [.unknown])
    
    let weeklyE = WeeklySchedule
        .empty
        .withSameBookingsOn(days: [.saturday, .sunday], times: [.unknown])
    
    
    func testGetBookings() {
        XCTAssertEqual(weeklyA.getBookingsFor(day: .monday), [])
        XCTAssertEqual(weeklyA.getBookingsFor(day: .tuesday), [.morning])
        XCTAssertEqual(weeklyA.getBookingsFor(day: .wednesday), [.morning, .afternoon])
        XCTAssertEqual(weeklyA.getBookingsFor(day: .thursday), [.morning])
        XCTAssertEqual(weeklyA.getBookingsFor(day: .friday), [])
        XCTAssertEqual(weeklyA.getBookingsFor(day: .saturday), [])
        XCTAssertEqual(weeklyA.getBookingsFor(day: .sunday), [])
        
        XCTAssertEqual(weeklyB.getBookingsFor(day: .monday), [])
        XCTAssertEqual(weeklyB.getBookingsFor(day: .tuesday), [.morning])
        XCTAssertEqual(weeklyB.getBookingsFor(day: .wednesday), [.morning, .afternoon])
        XCTAssertEqual(weeklyB.getBookingsFor(day: .thursday), [.morning])
        XCTAssertEqual(weeklyB.getBookingsFor(day: .friday), [])
        XCTAssertEqual(weeklyB.getBookingsFor(day: .saturday), [])
        XCTAssertEqual(weeklyB.getBookingsFor(day: .sunday), [])
        
        XCTAssertEqual(weeklyC.getBookingsFor(day: .monday), [.morning, .evening])
        XCTAssertEqual(weeklyC.getBookingsFor(day: .tuesday), [])
        XCTAssertEqual(weeklyC.getBookingsFor(day: .wednesday), [])
        XCTAssertEqual(weeklyC.getBookingsFor(day: .thursday), [])
        XCTAssertEqual(weeklyC.getBookingsFor(day: .friday), [.morning, .evening])
        XCTAssertEqual(weeklyC.getBookingsFor(day: .saturday), [])
        XCTAssertEqual(weeklyC.getBookingsFor(day: .sunday), [])
        
        XCTAssertEqual(weeklyD.getBookingsFor(day: .monday), [.unknown])
        XCTAssertEqual(weeklyD.getBookingsFor(day: .tuesday), [.unknown])
        XCTAssertEqual(weeklyD.getBookingsFor(day: .wednesday), [.unknown])
        XCTAssertEqual(weeklyD.getBookingsFor(day: .thursday), [.unknown])
        XCTAssertEqual(weeklyD.getBookingsFor(day: .friday), [.unknown])
        XCTAssertEqual(weeklyD.getBookingsFor(day: .saturday), [])
        XCTAssertEqual(weeklyD.getBookingsFor(day: .sunday), [])
    }
    
    func testUnion() {
        let unionA = weeklyA + weeklyB
        let unionB = weeklyA + weeklyC
        let unionC = weeklyC + weeklyD
        
        XCTAssertEqual(unionA.getBookingsFor(day: .monday), [])
        XCTAssertEqual(unionA.getBookingsFor(day: .tuesday), [.morning])
        XCTAssertEqual(unionA.getBookingsFor(day: .wednesday), [.morning, .afternoon])
        XCTAssertEqual(unionA.getBookingsFor(day: .thursday), [.morning])
        XCTAssertEqual(unionA.getBookingsFor(day: .friday), [])
        XCTAssertEqual(unionA.getBookingsFor(day: .saturday), [])
        XCTAssertEqual(unionA.getBookingsFor(day: .sunday), [])
        
        XCTAssertEqual(unionB.getBookingsFor(day: .monday), [.morning, .evening])
        XCTAssertEqual(unionB.getBookingsFor(day: .tuesday), [.morning])
        XCTAssertEqual(unionB.getBookingsFor(day: .wednesday), [.morning, .afternoon])
        XCTAssertEqual(unionB.getBookingsFor(day: .thursday), [.morning])
        XCTAssertEqual(unionB.getBookingsFor(day: .friday), [.morning, .evening])
        XCTAssertEqual(unionB.getBookingsFor(day: .saturday), [])
        XCTAssertEqual(unionB.getBookingsFor(day: .sunday), [])
        
        XCTAssertEqual(unionC.getBookingsFor(day: .monday), [.unknown, .morning, .evening])
        XCTAssertEqual(unionC.getBookingsFor(day: .tuesday), [.unknown])
        XCTAssertEqual(unionC.getBookingsFor(day: .wednesday), [.unknown])
        XCTAssertEqual(unionC.getBookingsFor(day: .thursday), [.unknown])
        XCTAssertEqual(unionC.getBookingsFor(day: .friday), [.unknown, .morning, .evening])
        XCTAssertEqual(unionC.getBookingsFor(day: .saturday), [])
        XCTAssertEqual(unionC.getBookingsFor(day: .sunday), [])

    }
    
    func testIntersection() {
        let intersectionA = weeklyA.intersection(weeklyB)
        let intersectionB = weeklyA.intersection(weeklyC)
        let intersectionC = weeklyC.intersection(weeklyD)
        
        XCTAssertEqual(intersectionA.getBookingsFor(day: .monday), [])
        XCTAssertEqual(intersectionA.getBookingsFor(day: .tuesday), [.morning])
        XCTAssertEqual(intersectionA.getBookingsFor(day: .wednesday), [.morning, .afternoon])
        XCTAssertEqual(intersectionA.getBookingsFor(day: .thursday), [.morning])
        XCTAssertEqual(intersectionA.getBookingsFor(day: .friday), [])
        XCTAssertEqual(intersectionA.getBookingsFor(day: .saturday), [])
        XCTAssertEqual(intersectionA.getBookingsFor(day: .sunday), [])
        
        XCTAssertEqual(intersectionB.getBookingsFor(day: .monday), [])
        XCTAssertEqual(intersectionB.getBookingsFor(day: .tuesday), [])
        XCTAssertEqual(intersectionB.getBookingsFor(day: .wednesday), [])
        XCTAssertEqual(intersectionB.getBookingsFor(day: .thursday), [])
        XCTAssertEqual(intersectionB.getBookingsFor(day: .friday), [])
        XCTAssertEqual(intersectionB.getBookingsFor(day: .saturday), [])
        XCTAssertEqual(intersectionB.getBookingsFor(day: .sunday), [])
        
        XCTAssert(intersectionB.isEmpty())
        
        //Intersectiong unknown and known (morning, afternoon, evening) will produce known
        XCTAssertEqual(intersectionC.getBookingsFor(day: .monday), [.morning, .evening])
        XCTAssertEqual(intersectionC.getBookingsFor(day: .tuesday), [])
        XCTAssertEqual(intersectionC.getBookingsFor(day: .wednesday), [])
        XCTAssertEqual(intersectionC.getBookingsFor(day: .thursday), [])
        XCTAssertEqual(intersectionC.getBookingsFor(day: .friday), [.morning, .evening])
        XCTAssertEqual(intersectionC.getBookingsFor(day: .saturday), [])
        XCTAssertEqual(intersectionC.getBookingsFor(day: .sunday), [])
     
        XCTAssert(!intersectionC.isEmpty())
    }
    
    func testClashes() {
        XCTAssert(weeklyA.maybeClashes(weeklyB))
        XCTAssert(weeklyA.surelyClashes(weeklyB))
        XCTAssert(!weeklyA.maybeClashes(weeklyC))
        XCTAssert(!weeklyA.surelyClashes(weeklyC))
        XCTAssert(weeklyC.maybeClashes(weeklyD))
        XCTAssert(!weeklyC.surelyClashes(weeklyD))
        XCTAssert(!weeklyC.maybeClashes(weeklyE))
        XCTAssert(!weeklyC.surelyClashes(weeklyE))
    }
    
    func testCheckAvailable() {
        XCTAssert(weeklyA.checkMaybeAvailable(day: .monday, time: .morning))
        XCTAssert(weeklyA.checkMaybeAvailable(day: .monday, time: .unknown))
        XCTAssert(weeklyA.checkSurelyAvailable(day: .monday, time: .morning))
        XCTAssert(weeklyA.checkSurelyAvailable(day: .monday, time: .unknown))
        
        XCTAssert(!weeklyA.checkMaybeAvailable(day: .wednesday, time: .morning))
        XCTAssert(weeklyA.checkMaybeAvailable(day: .wednesday, time: .unknown))
        XCTAssert(!weeklyA.checkSurelyAvailable(day: .wednesday, time: .morning))
        XCTAssert(weeklyA.checkSurelyAvailable(day: .wednesday, time: .unknown))
        
        XCTAssert(weeklyD.checkMaybeAvailable(day: .wednesday, time: .morning))
        XCTAssert(weeklyD.checkMaybeAvailable(day: .wednesday, time: .unknown))
        XCTAssert(!weeklyD.checkSurelyAvailable(day: .wednesday, time: .morning))
        XCTAssert(!weeklyD.checkSurelyAvailable(day: .wednesday, time: .unknown))
        
        XCTAssert(!weeklyA.checkBooked(day: .monday, time: .morning))
        XCTAssert(weeklyA.checkBooked(day: .wednesday, time: .morning))
    }

}
