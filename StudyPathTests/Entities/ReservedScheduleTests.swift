//
//  ReservedScheduleTests.swift
//  StudyPathTests
//
//  Created by iosdev on 25/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import XCTest
@testable import StudyPath

class ReservedScheduleTests: XCTestCase {

    let scheduleA = ReservedSchedule.empty.withReserved(year: 1, weeks: Array(10..<15), weeklySchedule: WeeklySchedule.empty.withSameBookingsEveryWorkingDay(times: [.morning, .afternoon]))
    
    let scheduleB = ReservedSchedule.empty.withReserved(year: 1, weeks: Array(20..<35), weeklySchedule: WeeklySchedule.empty.withSameBookingsEveryWorkingDay(times: [.morning]))
    
    let scheduleC = ReservedSchedule.empty.withReserved(year: 1, weeks: Array(12..<25), weeklySchedule: WeeklySchedule.empty.withSameBookingsEveryWorkingDay(times: [.morning]))
    
    let scheduleD = ReservedSchedule.empty.withReserved(year: 1, weeks: Array(15..<25), weeklySchedule: WeeklySchedule.empty.withSameBookingsEveryWorkingDay(times: [.unknown]))

    let scheduleE = ReservedSchedule.empty.withReserved(year: 2, weeks: Array(15..<25), weeklySchedule: WeeklySchedule.empty.withSameBookingsEveryWorkingDay(times: [.morning]))

    func testCompatible() {
        XCTAssert(!scheduleA.maybeClashes(scheduleB))
        XCTAssert(!scheduleA.surelyClashes(scheduleB))
        
        XCTAssert(scheduleA.maybeClashes(scheduleC))
        XCTAssert(scheduleA.surelyClashes(scheduleC))
        
        XCTAssert(scheduleC.maybeClashes(scheduleD))
        XCTAssert(!scheduleC.surelyClashes(scheduleD))
        
        XCTAssert(!scheduleA.maybeClashes(scheduleE))
        XCTAssert(!scheduleA.surelyClashes(scheduleE))
        XCTAssert(!scheduleB.maybeClashes(scheduleE))
        XCTAssert(!scheduleB.surelyClashes(scheduleE))
        XCTAssert(!scheduleC.maybeClashes(scheduleE))
        XCTAssert(!scheduleC.surelyClashes(scheduleE))
        XCTAssert(!scheduleD.maybeClashes(scheduleE))
        XCTAssert(!scheduleD.surelyClashes(scheduleE))
        
    }

}
