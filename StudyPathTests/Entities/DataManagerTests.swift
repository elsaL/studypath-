//
//  DataManagerTests.swift
//  StudyPathTests
//
//  Created by iosdev on 06/05/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import XCTest
@testable import StudyPath

class DataManagerTests: XCTestCase {

    let dm = DataManager(coursesJSON: "SampleCoursesTest",
                         modelJSON: "SampleModelCourseTest",
                         relevanceJSON: "RelevanceTableTest")
    
    func testJSONLoad() {
        XCTAssertEqual(dm.courses.count, 100)
        XCTAssertEqual(dm.models.count, 20)
        XCTAssertEqual(dm.relevanceRelationships.count, 20)
        
        let c1 = dm.courses.filter{$0.name == "Microsoft Azure Web Developer"}.first!
        let c21 = dm.courses.filter{$0.name == "Angular 7 (formerly Angular 2) - The Complete Guide"}.first!
        
        let c1ExpectedSchedule = ReservedSchedule.empty.withReserved(year: 0, weeks: Array(2..<15), weeklySchedule: WeeklySchedule.empty.withSameBookingsEveryWorkingDay(times: [.morning, .afternoon]))
        let c21ExpectedSchedule = ReservedSchedule.empty.withReserved(year: 0, weeks: Array(33..<49), weeklySchedule: WeeklySchedule.empty.withSameBookingsOn(days: [.monday, .wednesday, .friday], times: [.morning, .afternoon]))
        
        XCTAssertEqual(c1.tags, ["Azure", "Microsoft"])
        XCTAssertEqual(c1.schedule, c1ExpectedSchedule)
        XCTAssertEqual(c21.tags, ["Angular 2", "Angular 7"])
        XCTAssertEqual(c21.schedule, c21ExpectedSchedule)
        
        let m3 = dm.models.filter{$0.id == "m3"}.first!
        let m2 = dm.models.filter{$0.id == "m2"}.first!
        let m4 = dm.models.filter{$0.id == "m4"}.first!
        XCTAssertEqual(m3.topics, ["UI UX"])
        
        let m3Vertex = dm.modelGraph.findVertices(data: m3).first!
        let m3Prereq = dm.modelGraph.incomingEdges(vertex: m3Vertex).map{$0.data.id}
        
        XCTAssertEqual(Set<String>(m3Prereq), Set([m2.id, m4.id]))
        
        let m2Rel = dm.relevanceRelationship(completed: m2).map{$0.target.id}
        
        XCTAssertEqual(Set(m2Rel), Set(["m3", "m11", "m14"]))
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
