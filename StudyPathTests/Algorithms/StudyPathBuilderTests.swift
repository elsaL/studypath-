//
//  StudyPathBuilderTests.swift
//  StudyPathTests
//
//  Created by iosdev on 07/05/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import XCTest
@testable import StudyPath

class StudyPathBuilderTests: XCTestCase {

    let dm = DataManager(coursesJSON: "SampleCoursesTest",
                         modelJSON: "SampleModelCourseTest",
                         relevanceJSON: "RelevanceTableTest")
    
    let userPreferences = UserPreferences(username: "aaa", location: "Helsinki", deliveryMode: "Contact", schedulePreference: ["Morning", "Afternoon"], allowFarAway: false, allowPaid: false, plannedStudyHours: 200, topics: ["FrontEnd" : TopicRating(experience: 1.1, preference: 1.5)], models: ["FrontEnd 1": 2], tags: ["HTML"])
    
    func testExample() {
    
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
