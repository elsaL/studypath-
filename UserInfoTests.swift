import XCTest
@testable import StudyPath

/**
 Unit tests of UserInfo struct
 */


class UserInfoTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testComputePwdHash() {
        let user1 = UserInfo(username: "Joe", email: "", fullName: "", password: "abc123")
        let result:String = user1.passwordHash
        let expected:String = "6367c48dd193d56ea7b0baad25b19455e529f5ee"
        
        XCTAssertEqual(expected, result)
    }
    
    
    func testverifyPassword() {
        let user1 = UserInfo(username: "Joe", email: "", fullName: "", password: "abc123")
        
        XCTAssertTrue(user1.verifyPassword(password: "abc123"))
        XCTAssertFalse(user1.verifyPassword(password: "123"))
    }

    

}
