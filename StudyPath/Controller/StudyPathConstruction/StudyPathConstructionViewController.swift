import UIKit

/// The Controller for the study path construction view
class StudyPathConstructionViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    /// The algorithm used to build the study path
    private let pathAlgorithm = StudyPathBuilder(dataManager: DataManager.main)
    
    /// The courses suggested in this iteration
    private var inputCourses: [Course] = []
    /// The output study path at the state of this iteration
    private var newStudyPath: StudyPath = StudyPath.empty(targetHours: 0)

    
    /// Is the user seeing the View path (viewPath = true) or Pick Courses (viewPath = false)
    var viewPath: Bool = false
    
    /// Did the user pick at least one course
    var hasPicked = false
    
    ///course pick counter: how many courses a user has picked/added to study path
    var pickCounter = 0
    
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var progressBarTitle: UILabel!
    @IBOutlet weak var progressBar: UIProgressView!
    @IBOutlet weak var btnNextLevel: UIButton!
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var pickCoursesTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Load any saved study path for the current user
        let username = DataManager.main.loggedIn!
        let targetHours = Int(DataManager.main.retrieveUserPreferences(username: username)?.plannedStudyHours ?? "0") ?? 0
        
        let savedStudyPath = DataManager.main.retrieveStudyPath(username: username) ?? StudyPath.empty(targetHours: targetHours)
        
        newStudyPath = savedStudyPath.withTarget(targetHours: targetHours)
        
        inputCourses = pathAlgorithm.currentChoices(picked: newStudyPath.allCourses, maxCount: 3 + newStudyPath.targetHours / 100)
        
        configureProgressBar()
        showProgess()
        registerCellToTableView()
        enableNextLevelBtn()
    }
    
    /// Configure progress bar, update progress, show hours of courses picked/target hours
    private func configureProgressBar(){
        progressBar.transform = progressBar.transform.scaledBy(x: 1, y: 5)
        progressBarTitle.text = "Picked/Target(Hours): \(newStudyPath.hours)/\(newStudyPath.targetHours)"
        progressBar.progress = 0.0
    }
    
    /// Update the progress asynchronously
    private func showProgess(){
        perform(#selector(updateProgress), with: nil, afterDelay: 0.01)
    }
    
    /// Update the progress
    @objc private func updateProgress(){
        progressBarTitle.text = "Picked/Target(Hours): \(newStudyPath.hours)/\(newStudyPath.targetHours)"
        let progress:Float = min(Float(newStudyPath.hours) / Float(newStudyPath.targetHours), 1.0)
        progressBar.progress = progress
        if progress < 0.7 {
            progressBar.progressTintColor = #colorLiteral(red: 0.9407406449, green: 0.5254793167, blue: 0.480658114, alpha: 1)
        }else if progress < 1 {
            progressBar.progressTintColor = #colorLiteral(red: 0.7467899323, green: 0.3407750726, blue: 0.3045539856, alpha: 1)
        }else if progress == 1{
            progressBar.progressTintColor = #colorLiteral(red: 0.5404011011, green: 0.1557784677, blue: 0.1453632712, alpha: 1)
        }
    }
    
    /// When the user clicks on an element, go to Course Info view and show that element
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let maybeCourse = viewPath ? newStudyPath.course(at:indexPath.row) : inputCourses[indexPath.row]
        if let course = maybeCourse {
            DataManager.main.setViewingCourse(course, canAdd: false)
            performSegue(withIdentifier: "showCourseInfo", sender: nil)
        }
    }
    
    /// Register custom cell created separately to TableView
    fileprivate func registerCellToTableView() {
        let nib = UINib(nibName: "PickCourseTableViewCell", bundle: nil)
        pickCoursesTableView.register(nib, forCellReuseIdentifier: "PickCourseTableViewCell")
        let font = UIFont.systemFont(ofSize: 14)
        segmentControl.setTitleTextAttributes([NSAttributedString.Key.font: font], for: .normal)
    }

    
    /// Return the number of sections in the tableView
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    /// Return the number of rows in the tableView, according to which tab the user is viewing
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewPath ? newStudyPath.allCourses.count : inputCourses.count
    }
    
    /// Produce the cells for the tableView containing the courses as items
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = tableView.dequeueReusableCell(withIdentifier: "PickCourseTableViewCell", for: indexPath) as! PickCourseTableViewCell
        
        configureCell(indexPath, cell)
        
        return cell
    }
    
    
    /// Override to support conditional editing of the table view.
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Allow edit at PickCourse screen, but not ViewPath screen
        if !viewPath {
            return true
        }
        return false
    }
    
    /// Add a course to the study path, and save the change to the database
    private func addCourse(_ course: Course, cell: PickCourseTableViewCell) {
        self.newStudyPath = self.newStudyPath.with(course: course)
        self.showProgess()
        self.pickCounter += 1
        cell.courseTitle.textColor = #colorLiteral(red: 0.8788378835, green: 0.6719534397, blue: 0.5047522187, alpha: 1)
        self.pickCoursesTableView.reloadData()
        // The user must be logged in, if it is not, we are in an illegal state
        DataManager.main.saveStudyPath(self.newStudyPath, username: DataManager.main.loggedIn!)
        
    }
    
    /**
     * Configure swpie to edit functions: to add or remove(cancel) a course from StudyPath
     * course hours are updated while picking/canceling to update progress bar
     */
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let cell = tableView.cellForRow(at: indexPath) as! PickCourseTableViewCell
        let course = self.inputCourses[indexPath.row]
        
        let pick = UITableViewRowAction(style: .default, title: "Pick") { (action, indexPath) in
            let clashes = self.newStudyPath.clashes(schedule: course.schedule).clashesCount
            // Require at least one maybe and one surely clash to complain
            // Sure clashes are 5 times more severe than maybe clashes
            if (clashes.maybe + clashes.surely * 5 > 7){
                let clashCount = clashes.maybe + clashes.surely
                let alert = UIAlertController(title: "Warning", message: "This course might clash with \(clashCount) of your other courses schedule, do you want to proceed anyway?", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: {action in
                    self.addCourse(course, cell: cell)
                }))
                alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
                self.present(alert, animated: true)
            } else if !(self.newStudyPath.contains(course: course)){
                if self.pickCounter <= 2 {
                    self.addCourse(course, cell: cell)
                }else{
                    let alert = UIAlertController(title: "Warning", message: "Sure to pick \(self.pickCounter+1) courses from the same level?", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: {action in
                        self.addCourse(course, cell: cell)
                    }))
                    alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
                    self.present(alert, animated: true)
                }
            }
        }
        let cancel = UITableViewRowAction(style: .default, title: "Cancel") { (action, indexPath) in
            let cell = tableView.cellForRow(at: indexPath) as! PickCourseTableViewCell
            cell.courseTitle.textColor = UIColor.darkGray
            self.newStudyPath = (try? self.newStudyPath.withRemoval(course: course)) ?? self.newStudyPath
            self.showProgess()
            self.pickCounter -= 1
        }

        pick.backgroundColor = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
        cancel.backgroundColor = #colorLiteral(red: 0.9372549057, green: 0.3490196168, blue: 0.1921568662, alpha: 1)
        
        if newStudyPath.contains(course: course){
            return [cancel]
        } else {
            return [pick]
        }
    }
    
   
     func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    /// Configure a cell based on model data
    fileprivate func configureCell(_ indexPath: IndexPath, _ cell: PickCourseTableViewCell) {
        // Configure cell content
        cell.courseTitle.textColor = UIColor.darkGray
        
        
        let maybeCourse = viewPath ? newStudyPath.course(at:indexPath.row) : inputCourses[indexPath.row]
        
        guard let course = maybeCourse else {
            return
        }
        
        cell.courseTitle?.text = course.name
        cell.courseHours.text = "\(course.hours) hours"
        if course.contactTeaching {
            cell.contactOrOnline.text = "Contact"
        }else{
            cell.contactOrOnline.text = "Online"
        }
        
    
        // Fetch the image of the course
        let url = course.imageUrl.flatMap{URL(string: $0)}
        if let url = url {
            let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
                if let error = error {
                    print("Client error \(error)")
                }
                
                guard let data = data, error == nil, response != nil else {return}
                
                do {
                    let img = data
                    DispatchQueue.main.async {
                        cell.courseImg.image = UIImage(data: img)
                    }
                }
            }
            task.resume()
        }
        
        
        if segmentControl.selectedSegmentIndex == 0 {
            if newStudyPath.contains(course: course){
                cell.courseTitle.textColor = #colorLiteral(red: 0.8788378835, green: 0.6719534397, blue: 0.5047522187, alpha: 1)
            }
        }

    }
    
    
    /// Switch view between PickCourses and ViewPath by clicking on segment control bars
    @IBAction func switchView(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            enableNextLevelBtn()
            break
        case 1:
            enableDoneBtn()
            break
        default:
            btnNextLevel.isEnabled = true
            break
        }
    }
    
    @IBAction func doneClick(_ sender: Any) {
        // The user must be logged in, if it is not, we are in an illegal state
        DataManager.main.saveStudyPath(self.newStudyPath, username: DataManager.main.loggedIn!)
    }
    
    /// Click next level button to load nextlevel courses      //data hard-coded
    @IBAction func nextLevel(_ sender: UIButton) {
        if pickCounter == 0 {
            let alert = UIAlertController(title: "Alert", message: "Pick at least one course", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.present(alert, animated: true)
        }else{
            inputCourses = pathAlgorithm.currentChoices(picked: newStudyPath.allCourses, maxCount: 3 + newStudyPath.hours / 100)
            pickCounter = 0
            pickCoursesTableView.reloadData()
        }
    }
    
    /// Show PickCourse screen
    fileprivate func enableNextLevelBtn() {
        btnNextLevel.isEnabled = true
        btnNextLevel.setTitleColor(UIColor.white, for: .normal)
        btnNextLevel.backgroundColor = #colorLiteral(red: 0.3134735823, green: 0.3346351087, blue: 0.4090438783, alpha: 1)
        btnDone.isEnabled = false
        btnDone.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 0), for: .disabled)
        btnDone.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0)
        viewPath = false
        pickCoursesTableView.reloadData()
    }
    
    /// Show ViewPath screen
    fileprivate func enableDoneBtn() {
        btnDone.isEnabled = true
        btnDone.setTitleColor(UIColor.white, for: .normal)
        btnDone.backgroundColor = #colorLiteral(red: 0.3134735823, green: 0.3346351087, blue: 0.4090438783, alpha: 1)
        btnNextLevel.isEnabled = false
        btnNextLevel.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 0), for: .disabled)
        btnNextLevel.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0)
        viewPath = true
        pickCoursesTableView.reloadData()

    }
    
   
}
