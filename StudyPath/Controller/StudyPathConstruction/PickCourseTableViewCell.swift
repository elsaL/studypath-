import UIKit

/// The table view cell for the Picked Course tab in Study Path Construction view
class PickCourseTableViewCell: UITableViewCell {

    @IBOutlet weak var courseImg: UIImageView!
    @IBOutlet weak var courseTitle: UILabel!
    @IBOutlet weak var contactOrOnline: UILabel!
    @IBOutlet weak var courseHours: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
