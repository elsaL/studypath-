//
//  ViewController.swift
//  StudyPath
//
//  Created by iosdev on 16/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import UIKit

/// The controller for the Home view
class HomeViewController: UIViewController {

    @IBOutlet weak var createStudyPathBtn: UIButton!
    
    @IBOutlet weak var currentStudyPathBtn: UIButton!
    
    @IBOutlet weak var redoOnboardingBtn: UIButton!
    
    @IBOutlet weak var searchCoursesBtn: UIButton!
    
    @IBOutlet weak var logoutBtn: UIButton!
    
    @IBAction func logout(_ sender: Any) {
        DataManager.main.logout()
        performSegue(withIdentifier: "toLoginSegue", sender: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Load the localized text into the UI
        createStudyPathBtn.setTitle(Strings.Home.createStudyPath, for: .normal)
        currentStudyPathBtn.setTitle(Strings.Home.currentStudyPath, for: .normal)
        searchCoursesBtn.setTitle(Strings.Home.searchCourses, for: .normal)
        redoOnboardingBtn.setTitle(Strings.Home.redoOnboarding, for: .normal)
        logoutBtn.setTitle(Strings.Home.logout, for: .normal)
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    

}

