//
//  AppInfoViewController.swift
//  StudyPath
//
//  Created by Andrei Vasilev on 03/05/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import UIKit

/// Controller for App Info screen (first screen of Onboarding)
class AppInfoViewController: UIViewController {

    @IBOutlet weak var welcomeLabel: UILabel!
    @IBOutlet weak var introLabel: UILabel!
    @IBOutlet weak var startButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        welcomeLabel.text = Strings.Onboarding.welcome
        introLabel.text = Strings.Onboarding.learnITIntro
        startButton.setTitle(Strings.start, for: .normal)
    }

}
