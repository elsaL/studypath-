//
//  TopicsViewController.swift
//  StudyPath
//
//  Created by Andrei Vasilev on 27/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import UIKit

/// Controller for topic rating in Onboarding
class TopicsViewController: UIViewController {
    
    var dbmodel = OnboardingModel(courses: DataManager.main.courses)
    
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    /// Go to model rating after topic rating
    @IBAction func pressNextBtn(_ sender: Any) {
        dbmodel.createScoredModelList()
        performSegue(withIdentifier: "showModels", sender: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.allowsSelection = true
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.showsVerticalScrollIndicator = false
        
        // Retrieve the initial topics from the course database
        dbmodel.populateRatedTopicListWithInitialValues(courses: dbmodel.existingCourses)
        
        // Set the localized text of the UI elements
        nextButton.setTitle(Strings.next, for: .normal)
        backButton.setTitle(Strings.back, for: .normal)
        titleLabel.text = Strings.Onboarding.questionTopics
        
    }
}

extension TopicsViewController:  UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dbmodel.ratedTopicList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "topicCell", for: indexPath) as! TopicCollectionViewCell;
        cell.layer.borderColor = #colorLiteral(red: 0.7540688515, green: 0.7540867925, blue: 0.7540771365, alpha: 1)
        cell.layer.borderWidth = 1
        cell.layer.cornerRadius = 15
        cell.layer.masksToBounds = true
        cell.layer.shadowColor = UIColor.lightGray.cgColor
        cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        cell.layer.shadowRadius = 6.0
        cell.layer.shadowOpacity = 1.0
        cell.layer.masksToBounds = false
        cell.topicTitleLabel.text = Array(dbmodel.ratedTopicList.keys)[indexPath[1]]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! TopicCollectionViewCell
        cell.backgroundColor = #colorLiteral(red: 0.7541063428, green: 0.3450443149, blue: 0.2997466326, alpha: 1)
        cell.topicTitleLabel.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        cell.contentView.layer.cornerRadius = 15
        cell.contentView.layer.masksToBounds = true
        showMenu(indexPath: indexPath)
    }
    
    /// Show the menu to rate a topic
    func showMenu(indexPath: IndexPath) {
        let topicTitle = Array(dbmodel.ratedTopicList.keys)[indexPath[1]]
        guard let experienceLevel = dbmodel.ratedTopicList[topicTitle]?.0 else { return }
        guard let preferLevel = dbmodel.ratedTopicList[topicTitle]?.1 else { return }
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.title = topicTitle
        
        let vc = UIViewController()
        let alertWidth = alert.view.bounds.width
        let experienceLabel = UILabel(frame:   CGRect( x:10, y: 0, width: alertWidth - 20, height: 20))
        let preferLabel = UILabel(frame:   CGRect( x:10, y: 120, width: alertWidth - 20, height: 20))
        
        /// Label settings
        experienceLabel.text = Strings.Onboarding.experienceMessage
        experienceLabel.textAlignment = .center
        experienceLabel.textColor = #colorLiteral(red: 0.2605174184, green: 0.2605243921, blue: 0.260520637, alpha: 1)
        experienceLabel.numberOfLines = 0
        preferLabel.text = Strings.Onboarding.preferenceMessage
        preferLabel.textAlignment = .center
        preferLabel.textColor = #colorLiteral(red: 0.2605174184, green: 0.2605243921, blue: 0.260520637, alpha: 1)
        preferLabel.numberOfLines = 0
        
        vc.preferredContentSize = CGSize(width: alertWidth ,height: 210)
        
        alert.isModalInPopover = true
        
        // Setup the slider for user preference and experience
        let preferSliderFrame = UISlider(frame: CGRect(x: 0, y: 140, width: alertWidth * 0.75,  height: 80))
        let experienceSliderFrame = UISlider(frame: CGRect(x: 0, y: 20, width: alertWidth * 0.75, height: 80))
        
        preferSliderFrame.tintColor = #colorLiteral(red: 0.7541063428, green: 0.3450443149, blue: 0.2997466326, alpha: 1)
        experienceSliderFrame.tintColor = #colorLiteral(red: 0.7541063428, green: 0.3450443149, blue: 0.2997466326, alpha: 1)
        preferSliderFrame.center.x = vc.view.center.x
        experienceSliderFrame.center.x = vc.view.center.x
        
        preferSliderFrame.maximumValue = 2
        preferSliderFrame.minimumValue = 0
        
        preferSliderFrame.setValue(Float(preferLevel), animated: false)
        
        experienceSliderFrame.maximumValue = 2
        experienceSliderFrame.minimumValue = 0
        experienceSliderFrame.setValue(Float(experienceLevel), animated: true)
        
        vc.view.addSubview(experienceLabel)
        vc.view.addSubview(experienceSliderFrame)
        vc.view.addSubview(preferLabel)
        vc.view.addSubview(preferSliderFrame)
        
        alert.setValue(vc, forKey: "contentViewController")
        alert.addAction(UIAlertAction(title: Strings.cancel, style: .cancel, handler: nil))
        
        // Rate the topic
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (UIAlertAction) in
            let newExperienceLevel = Double(experienceSliderFrame.value)
            let newPreferLevel = Double(preferSliderFrame.value)
            self.dbmodel.updateRatedTopicList(key: topicTitle, exp: newExperienceLevel, pref: newPreferLevel)
        }))
        
        self.present(alert,animated: true, completion: nil )
    }

    /// Send the data to the model rating view (ModelViewController)
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showModels" {
            let mvc = segue.destination as! ModelViewController
            mvc.dbModel = self.dbmodel
        }
    }
}



