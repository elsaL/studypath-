//
//  SurveyViewController.swift
//  StudyPath
//
//  Created by Andrei Vasilev on 27/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import UIKit
import CoreData

/// Controller for the final onboarding step, where the user pick additional information on their preferences
class SurveyViewController: UIViewController {
    
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var oneMoreStepLabel: UILabel!
    @IBOutlet weak var doneBtn: UIButton!
    @IBOutlet weak var hoursTextField: UITextField!
    @IBOutlet weak var studyTime: UITextField!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var locationInputField: UITextField!
    @IBOutlet weak var educationLengthLabel: UILabel!
    @IBOutlet weak var distancePreferenceLabel: UILabel!
    @IBOutlet weak var distancePreferenceSwitch: UISwitch!
    @IBOutlet weak var hoursPerDayLabel: UILabel!
    @IBOutlet weak var paidPreferenceLabel: UILabel!
    @IBOutlet weak var paidPreferenceSwitch: UISwitch!
    @IBOutlet weak var courseTypeLabel: UILabel!
    @IBOutlet weak var courseTypePickerView: UIPickerView!
    @IBOutlet weak var morningLabel: UILabel!
    @IBOutlet weak var afternoonLabel: UILabel!
    @IBOutlet weak var eveningLabel: UILabel!
    @IBOutlet weak var schedulePreferenceLabel: UILabel!

    // The possible types of course
    let courseTypes: [String] = ["Online", "Contact"]
    // The schedule preference selected by the user (can contain "Morning", "Afternoon" and "Evening")
    var scheduleTypes: [String] = []
    // The model for the onboarding process
    var dbModel: OnboardingModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        courseTypePickerView.dataSource = self
        courseTypePickerView.delegate = self
        
        // Load localized strings into the UI
        backBtn.setTitle(Strings.back, for: .normal)
        doneBtn.setTitle(Strings.done, for: .normal)
        morningLabel.text = Strings.Onboarding.morning
        afternoonLabel.text = Strings.Onboarding.afternoon
        eveningLabel.text = Strings.Onboarding.evening
        schedulePreferenceLabel.text = Strings.Onboarding.selectSchedulePreference
        courseTypeLabel.text = Strings.Onboarding.courseType
        paidPreferenceLabel.text = Strings.Onboarding.paidPreference
        distancePreferenceLabel.text = Strings.Onboarding.distancePreference
        hoursPerDayLabel.text = Strings.Onboarding.hoursPerDay
        educationLengthLabel.text = Strings.Onboarding.educationLength
        locationInputField.placeholder = Strings.Onboarding.location
        locationLabel.text = Strings.Onboarding.locationInput
        oneMoreStepLabel.text = Strings.Onboarding.oneMoreStep
        
    }
    
    /// The user has selected one of the checkboxes to choose its favourite study time
    @IBAction func checkMarkTapped(_ sender: UIButton) {
        if (sender.isSelected == true)
        {
            sender.isSelected = false;
            print(sender.tag)
            switch sender.tag {
            case 1:
                scheduleTypes = scheduleTypes.filter({ $0 != "Morning"})
            case 2:
                scheduleTypes = scheduleTypes.filter({ $0 != "Afternoon"})
            case 3:
                scheduleTypes = scheduleTypes.filter({ $0 != "Evening"})
            default:
                return
            }
        }
        else
        {
            sender.isSelected = true;
            switch sender.tag {
            case 1:
                scheduleTypes.append("Morning")
            case 2:
                scheduleTypes.append("Afternoon")
            case 3:
                scheduleTypes.append("Evening")
            default:
                return
            }
        }
    }
    
    @IBAction func goBack(_ sender: Any) {
        dbModel.ratedTagList.removeAll()
        performSegue(withIdentifier: "backFromSurvey", sender: self)
    }
    
    @IBAction func onPressDoneBtn(_ sender: Any) {
        
        if locationInputField.text!.isEmpty || hoursTextField.text!.isEmpty || studyTime.text!.isEmpty {
            showAlert()
            return
        }
        
        /*
         This should never be nil. If it is, we are in an illegal state and the app should crash.
         */
        let username = DataManager.main.loggedIn!
        let location = locationLabel.text ?? "Helsinki"
        let deliveryMode = courseTypes[courseTypePickerView.selectedRow(inComponent: 0)]
        let months = Int(studyTime.text ?? "24") ?? 24
        let hours = Int(hoursTextField.text ?? "5") ?? 5
        let totalHours = months * 20 * hours
        let topics = dbModel.ratedTopicList
            .mapValues{pair in TopicRating(experience: pair.0, preference: pair.1)}
        let models = dbModel.ratedModelList
        let tags = dbModel.ratedTagList
        
        let userPrefs = UserPreferences(username: username, location: location, deliveryMode: deliveryMode, schedulePreference: scheduleTypes, allowFarAway: distancePreferenceSwitch.isOn, allowPaid: paidPreferenceSwitch.isOn, plannedStudyHours: totalHours, topics: topics, models: models, tags: tags)
        
        // Save the result of the Onboarding into the database
        DataManager.main.saveUserPreferences(userPrefs)
        
        performSegue(withIdentifier: "homeSegue", sender: self)
        
    }
    
    private func showAlert() {
        let alert = UIAlertController(title: Strings.Onboarding.fillAllField, message: nil, preferredStyle: .alert)
        let okBtn = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
        alert.addAction(okBtn)
        present(alert, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "backFromSurvey" {
            let mvc = segue.destination as! TagsViewController
            mvc.dbModel = self.dbModel
        }
    }
}



extension SurveyViewController: UIPickerViewDataSource, UIPickerViewDelegate  {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView.tag {
        case 1:
            return courseTypes.count
        default:
            return scheduleTypes.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView.tag {
        case 1:
            return String(courseTypes[row])
        default:
            return String(scheduleTypes[row])
        }
    }
}

