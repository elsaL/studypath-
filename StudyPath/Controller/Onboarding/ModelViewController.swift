//
//  ModelViewController.swift
//  StudyPath
//
//  Created by Andrei Vasilev on 29/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import UIKit

/// Controller for model rating in Onboarding
class ModelViewController: UIViewController {
    
    // The model for the onboarding process
    var dbModel: OnboardingModel!
    
    @IBOutlet weak var modelTitleLb: UILabel!
    var sortedModelList :[(key: String, value: Double)] = []
    @IBOutlet weak var mCollectionView: UICollectionView!
    @IBOutlet var nextBtn: UIView!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mCollectionView.delegate = self
        mCollectionView.dataSource = self
        
        // Sort the models in the database by score, computed from their topic rating from previous screen
        sortedModelList = dbModel.modelListWithScores.sorted(by: { $0.value > $1.value })
        if sortedModelList.count > 12 {
            sortedModelList = Array(sortedModelList[0 ..< 13])
        }
        
        // Set the localized text for the UI elements
        modelTitleLb.text = Strings.Onboarding.modelQuestion
        nextButton.setTitle(Strings.next, for: .normal)
        backButton.setTitle(Strings.back, for: .normal)
    }
    
    /// Notify the user that they have to select at least one model
    private func showAlert() {
        let alert = UIAlertController(title: Strings.Onboarding.selectSomethingFirst, message: nil, preferredStyle: .alert)
        let okBtn = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
        alert.addAction(okBtn)
        present(alert, animated: true)
    }
    
    @IBAction func onPressNextBtn(_ sender: Any) {
        if dbModel.ratedModelList.count == 0 {
            showAlert()
            return
        }
        dbModel.pupulateRelevantTagList()
        performSegue(withIdentifier: "showTags", sender: self)
    }
    
    /// Menu allowing the user to rate a model
    func showAlertMenu(for cell: ModelCollectionViewCell, indexPath: IndexPath) {
        let alert = UIAlertController(title: Strings.Onboarding.modelLike, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: Strings.cancel, style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: Strings.Onboarding.like, style: .default, handler: { (UIAlertAction) in
            cell.unicodeLabel.text = "👍"
            cell.unicodeLabel.isHidden = false
            self.dbModel.populateRatedModelList(title: self.sortedModelList[indexPath[1]].key, grade: 1)
        }))
        alert.addAction(UIAlertAction(title: Strings.Onboarding.love, style: .default, handler: { (UIAlertAction) in
            cell.unicodeLabel.text = "❤️"
            cell.unicodeLabel.isHidden = false
            self.dbModel.populateRatedModelList(title: self.sortedModelList[indexPath[1]].key, grade: 2)
        }))
        alert.addAction(UIAlertAction(title: Strings.Onboarding.passion, style: .default, handler: { (UIAlertAction) in
            cell.unicodeLabel.text = "🔥"
            cell.unicodeLabel.isHidden = false
            self.dbModel.populateRatedModelList(title: self.sortedModelList[indexPath[1]].key, grade: 3)
            
        }))
        alert.addAction(UIAlertAction(title: Strings.Onboarding.reset, style: .default, handler: { (UIAlertAction) in
            cell.unicodeLabel.isHidden = true
            self.dbModel.populateRatedModelList(title: self.sortedModelList[indexPath[1]].key, grade: 0)
            
        }))
        self.present(alert,animated: true, completion: nil )
    }
    
    /// Pass the data to the tags selection step (TagViewController)
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showTags" {
            let mvc = segue.destination as! TagsViewController
            mvc.dbModel = self.dbModel
        }
    }
    
}


extension ModelViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return sortedModelList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "modelCell", for: indexPath) as! ModelCollectionViewCell;
        cell.layer.cornerRadius = 15
        cell.layer.borderColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        cell.layer.borderWidth = 1
        cell.unicodeLabel.isHidden = true
        cell.modleTitleLabel.text = sortedModelList[indexPath[1]].key
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! ModelCollectionViewCell
        self.showAlertMenu(for: cell, indexPath: indexPath)
    }
}
extension ModelViewController: UICollectionViewDelegate {}

