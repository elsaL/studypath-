//
//  TagsViewController.swift
//  StudyPath
//
//  Created by Andrei Vasilev on 28/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import UIKit

/// The controller for the onboarding step that allows the user to select favourite tags
class TagsViewController: UIViewController {
    // The model of the onboarding process
    var dbModel: OnboardingModel!
    // The list of tags to be shown to the user. Computer from previous Onboarding steps.
    var tagList = [String]()
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var tagsTitle: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tagList = dbModel.relevantTagList
        if tagList.count > 12 {
            tagList = Array(tagList[0 ..< 13])
        }
        backBtn.setTitle(Strings.back, for: .normal)
        nextButton.setTitle(Strings.next, for: .normal)
        tagsTitle.text = Strings.Onboarding.selectTags
    }
    
    @IBAction func goBack(_ sender: Any) {
        dbModel.relevantTagList.removeAll()
        dbModel.ratedModelList.removeAll()
        performSegue(withIdentifier: "backFromTags", sender: self)
    }
    
    @IBAction func onPressNextBtn(_ sender: Any) {
        if dbModel.ratedTagList.count == 0 {
            showAlert()
        } else {
            performSegue(withIdentifier: "showSurvey", sender: self)
        }
    }
    
    /// The user has clicked a tag, so update the state of the tags
    @objc func switchChanged(_ sender : UISwitch!){
        let tag = tagList[sender.tag]
        dbModel.populateRatedTagList(tag: tag)
        print(dbModel.ratedTagList)
    }
    
    /// Send the data forward or backward in the Onboarding process
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showSurvey" {
            let mvc = segue.destination as! SurveyViewController
            mvc.dbModel = self.dbModel
        }
        
        if segue.identifier == "backFromTags" {
            let mvc = segue.destination as! ModelViewController
            mvc.dbModel = self.dbModel
        }
        
    }
    
    private func showAlert() {
        let alert = UIAlertController(title: Strings.Onboarding.selectSomethingFirst, message: nil, preferredStyle: .alert)
        let okBtn = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
        alert.addAction(okBtn)
        present(alert, animated: true)
    }
}

extension TagsViewController: UITableViewDelegate {}

extension TagsViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tagList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tagCell", for: indexPath) as! TagsTableViewCell
        let tagTitle = tagList[indexPath.row]
        cell.switchTag.isOn = dbModel.ratedTagList.contains(tagTitle)
        cell.tagLabel.text = tagTitle
        cell.selectionStyle = .none
        cell.switchTag.tag = indexPath.row
        cell.switchTag.addTarget(self, action: #selector(self.switchChanged(_: )), for: .valueChanged)
        return cell
    }
}
