//
//  CourseInfoController.swift
//  StudyPath
//
//  Created by iosdev on 28/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import UIKit

/// Controller for Course Info view
internal class CourseInfoController : UIViewController {
    
    @IBOutlet weak var courseImage: UIImageView!
    @IBOutlet weak var courseTitle: UILabel!
    @IBOutlet weak var courseHours: UILabel!
    @IBOutlet weak var courseRating: UILabel!
    @IBOutlet weak var courseInstitution: UILabel!
    @IBOutlet weak var courseLocation: UILabel!
    @IBOutlet weak var courseLanguage: UILabel!
    @IBOutlet weak var courseMode: UILabel!
    @IBOutlet weak var courseLevel: UILabel!
    @IBOutlet weak var addToPathButton: UIButton!
    @IBOutlet weak var courseStart: UILabel!
    @IBOutlet weak var prerequisiteTitle: UILabel!
    @IBOutlet weak var prerequisiteText: UITextView!
    @IBOutlet weak var descriptionTitle: UILabel!
    @IBOutlet weak var descriptionText: UITextView!
    @IBOutlet weak var technologiesTitle: UILabel!
    @IBOutlet weak var technologiesText: UITextView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    override internal func viewDidLoad() {
        super.viewDidLoad()
        if let course = DataManager.main.currentlyViewingCourse {
            /**
             * Hide the add to path button if we are coming from the study path building screen,
             * since otherwise the user could bypass the constructin mechanism.
             */
            let canAdd = DataManager.main.currentlyViewingCourseCanAdd
            addToPathButton.isHidden = !canAdd
            displayCourse(course: course)
        }
    }
    
    /// Display the course info of a course
    func displayCourse(course: Course) {
        if let strUrl = course.imageUrl, let url = URL(string: strUrl) {
            downloadImage(with: url)
        }
        
        let levels = [0:"entry", 1:"beginner", 2:"intermediate", 3:"advanced"]
        
        let provider = course.provider == "" ? "unknown" : course.provider
        let mode = course.contactTeaching ? "contact" : "online"
        let level = levels[course.model.level] ?? "unknown"
        let startsOn = course.schedule.startingWeek().map{String("week \($0+1)")} ?? "unknown"
        /// Find all the prerequisites
        let prerequisites = DataManager.main.modelGraph.findVertices(data: course.model).first.map{vertex in
            DataManager.main.modelGraph.incomingEdges(vertex: vertex).map{$0.data.name}
        }
        let firstPrereq = prerequisites?.first ?? "none"
        let otherPrereq = prerequisites?.dropFirst()
        
        let firstTag = course.tags.first ?? ""
        let otherTags = course.tags.dropFirst()
        
        let firstLang = course.language.first ?? ""
        let otherLangs = course.language.dropFirst()
        
        courseTitle.text = course.name
        courseHours.text = "\(course.hours) hours"
        courseInstitution.text = "\(provider)"
        courseLocation.text = "\(course.location)"
        courseLanguage.text = "\(otherLangs.reduce(firstLang, {"\($0), \($1)"}))"
        courseMode.text = "\(mode)"
        courseLevel.text = "\(level)"
        courseStart.text = "Starts: \(startsOn)"
        prerequisiteTitle.text = "Prerequisite"
        prerequisiteText.text = otherPrereq?.reduce(firstPrereq, {"\($0), \($1)"}) ?? "none"
        descriptionTitle.text = "Description"
        descriptionText.text = course.description
        technologiesTitle.text = "Tags"
        technologiesText.text = otherTags.reduce(firstTag, {"\($0), \($1)"})
        
    }
    
    /// Show the info alert telling the user that the course has been added
    private func showAdded() {
        let alert = UIAlertController(title: "Info", message: "Course added to your study path.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true)
    }
    
    /**
     * Try to add a course in the user's study path.
     * Warn the user in case of conflicts or missing prerequisites.
     */
    private func tryToAdd(course: Course, studyPath: StudyPath, user: String){
        let clashes = studyPath.clashes(schedule: course.schedule).clashesCount
        // Require at least one maybe and one surely clash to complain.
        // Sure clashes are 5 times more severe than maybe clashes.
        if (clashes.maybe + clashes.surely * 5 > 7){
            let clashCount = clashes.maybe + clashes.surely
            let alert = UIAlertController(title: "Warning", message: "This course might clash with \(clashCount) of your other courses schedule, do you want to proceed anyway?", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: {action in
                DataManager.main.saveStudyPath(studyPath, username: user)
                self.showAdded()
                return
            }))
            alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
            self.present(alert, animated: true)
        } else {
            // No clashes, just add the course
            DataManager.main.saveStudyPath(studyPath, username: user)
            showAdded()
            return
        }
    }
    
    /**
     * Executed when the user tries to add the viewed course to its study path.
     * Check for prerequisites and clashes, and warn the user in case they are present.
     */
    @IBAction func addToPathClick(_ sender: Any) {
        if
            let course = DataManager.main.currentlyViewingCourse,
            let user = DataManager.main.loggedIn,
            let studyPath = DataManager.main.retrieveStudyPath(username: user) {
            if (studyPath.contains(course: course)){
                showAdded()
                return
            }
            
            let models = Set(studyPath.allCourses.map{$0.model.id})
            let vertex = DataManager.main.modelGraph.allVertices.filter{$0.data.id == course.model.id}.first
            let prereqs = (vertex.map{DataManager.main.modelGraph.incomingEdges(vertex: $0)} ?? []).map{$0.data.id}
            let updatedStudyPath = studyPath.with(course: course)
            
            if !(models.contains(where: {model in prereqs.contains(model)})) {
                let alert = UIAlertController(title: "Warning", message: "You might not have all the learning prerequisites for this course. Do you want to continue anyway?", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: {action in
                    self.tryToAdd(course: course, studyPath: updatedStudyPath, user: user)
                }))
                alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
                self.present(alert, animated: true)
            } else {
                // No missing prerequisite, so try to add the course (checking for clashes)
                tryToAdd(course: course, studyPath: updatedStudyPath, user: user)
            }
        }
    }
    
    /// Download the image of the course
    func downloadImage(with url: URL) {
        spinner(spin: true)
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let error = error {
                print("Client error \(error)")
            }
            
            guard let data = data, error == nil, response != nil else {return}
            
            do {
                let img = data
                DispatchQueue.main.async {
                    self.courseImage.image = UIImage(data: img)
                    self.spinner(spin: false)
                }
            }
        }
        task.resume()
        
    }
    
    /// Show or hide a spinner to represent image loading
    func spinner(spin status: Bool) {
        if status == true {
            spinner.isHidden = false
            spinner.startAnimating()
        }else {
            spinner.isHidden = true
            spinner.stopAnimating()
        }
    }
}

