import UIKit
import CoreData

/**
 Signup view controller with
 - textfield validation
 - signup function
 - save User to core data if successfully created account
 */
class SignupViewController: UIViewController {
    
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var signupButton: UIButton!
    @IBOutlet weak var signupGoogleButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    /**
     Signup function with textfield validation:
     - check if given username exists: fetch from coredata with given username
     - username needs to me more than 2 characters (made simple for initial use)
     - email format needs to be correct
     - password needs to be more than 2 characters (made simple for initial use)
     - confirm password to make sure it matches with the first one
     */
    @IBAction func signup(_ sender: Any) {
        if let username = self.usernameTextField.text {
            
            if DataManager.main.fetchUserByUsername(username: username) != nil {
                showAlert(message: "Username exists already")
            }
            
            if username.count < 3 {
                showAlert(message: "Username needs to be more than 2 characters")
                return
            }
            
            if let email = self.emailTextField.text {
                if email.isEmpty {
                    showAlert(message: "Email cannot be empty")
                    return
                }
                if !isValidEmail(email: email){
                    showAlert(message: "Email format is not correct")
                    return
                }
                if let password = self.passwordTextField.text {
                    if password.count < 3 {
                        showAlert(message: "Password needs to be more than 2 characters")
                        return
                    }
                    
                    if let confirmPwd = self.confirmPasswordTextField.text {
                        if confirmPwd != password {
                            showAlert(message: "Passwords do not match")
                            return
                        }
                        
                        DataManager.main.saveUser(username, email, password)
                        DataManager.main.login(username: username)
                        performSegue(withIdentifier: "toOnboardingSegue", sender: self)
                        
                        
                        clearTextField()
                        
                    }else{
                        fatalError("error from text field")
                    }
                }else{
                    fatalError("error from text field")
                }
            }else{
                fatalError("error from text field")
            }
        }else{
            fatalError("error from text field")
        }
    }
    
    
    
    /**
     Validate if given email address is in correct formate
     - parameter email:
     */
    private func isValidEmail(email:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
    
    
    /**
     Clear textfield input after signup
     */
    private func clearTextField() {
        self.usernameTextField.text = ""
        self.emailTextField.text = ""
        self.passwordTextField.text = ""
        self.confirmPasswordTextField.text = ""
    }
    
    /**
     Show alert message
     - parameter message:
     */
    private func showAlert(message:String)  {
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
    
}


