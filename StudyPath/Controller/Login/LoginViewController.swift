import UIKit
import CoreData

/**
 Represent login viewcontroller with
 - login authentication
 - alert messages
 
 */

class LoginViewController: UIViewController {
    @IBOutlet weak var tosLabelA: UILabel!
    @IBOutlet weak var tosLabelB: UIButton!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var joinFreeBtn: UIButton!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (DataManager.main.loggedIn != nil) {
            performSegue(withIdentifier: "toHomeSegue", sender: self)
        }
        
        // Load localized strings into the UI
        tosLabelA.text = Strings.Login.tosA
        tosLabelB.setTitle(Strings.Login.tosB, for: .normal)
        loginBtn.setTitle(Strings.Login.login, for: .normal)
        joinFreeBtn.setTitle(Strings.Login.join, for: .normal)
        usernameTextField.placeholder = Strings.Login.username
        passwordTextField.placeholder = Strings.Login.password
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    /**
     Login function:
     Check if the given username matches with any username from record
     if yes, check if given password matches with the password associated with the username from record
     */
    @IBAction func login(_ sender: Any) {
        
        if let username = self.usernameTextField.text {
            
            let user =  DataManager.main.fetchUserByUsername(username: username)
            
            if user == nil {
                showAlert(message: Strings.Login.usernameDoesNotExistError)
                return
            }
            
            if let password = self.passwordTextField.text {
                if  user?.username == username {
                    let hashMatch = user?.verifyPassword(password: password) ?? false
                    if hashMatch {
                            DataManager.main.login(username: username)
                            let onboardingData = DataManager.main.retrieveUserPreferences(username: username)
                        if onboardingData != nil {
                            performSegue(withIdentifier: "toHomeSegue", sender: self)
                            return
                        }
                            performSegue(withIdentifier: "onboardingSegue", sender: self)
                            return
                        }else{
                            showAlert(message: Strings.Login.passwordIncorrectError)
                            return
                    }
                } else {
                    showAlert(message: Strings.Login.usernameDoesNotExistError)
                }
            }else{
                fatalError("system error")
            }
        }else{
            fatalError("system error")
        }
           
    
    }
    
    /**
     Show alert messages during login
     - parameter message: the alert message
     */
    private func showAlert(message:String)  {
        let alert = UIAlertController(title: Strings.alert, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: Strings.ok, style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }

    /**
     Clear textfield input on login screen if user clicks CREATE ACCOUNT button
     */
    @IBAction func clearTextField(_ sender: Any) {
        self.usernameTextField.text = ""
        self.passwordTextField.text = ""
    }
   
}
