import UIKit

// TODO: This should be renamed to SearchViewController

/// The controller for the Search screen
class ViewController: UITableViewController, UISearchResultsUpdating {
    
    /// Search results
    var results:[Course] = []
    
    /// The controller for the search functionality
    let searchController = UISearchController(searchResultsController:nil)
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set the delegates
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        definesPresentationContext = true
        tableView.tableHeaderView = searchController.searchBar
        
        
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return results.count
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let courseSelected = results[indexPath.row]
        DataManager.main.setViewingCourse(courseSelected)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as? TableCell else {return UITableViewCell()}

        // Set the text of the cell
        let courseDisplayed = results[indexPath.row]
        cell.nameLbl?.text = courseDisplayed.name

        // Fetch the image of the course
        if let imageURL = URL(string: courseDisplayed.imageUrl ?? "") {
            let task = URLSession.shared.dataTask(with: imageURL) { (data, response, error) in
                if let error = error {
                    print("Client error \(error)")
                }
                guard let data = data, error == nil, response != nil else {return}
                do {
                    let img = data
                    DispatchQueue.main.async {
                        cell.imgView.image = UIImage(data: img)
                        self.tableView.reloadData()
                    }
                }
            }
            task.resume()
        }
        
        
        return cell
    }
    
    /// Show the search result when the user writes
    func updateSearchResults(for searchController: UISearchController) {
     
        let words = searchController.searchBar.text!.lowercased().split(separator: " ")
        //Search for the user inserted words in the title and tags of the courses
        results = DataManager.main.courses.filter{course in words.allSatisfy{word in course.name.lowercased().contains(word) || course.tags.map{$0.lowercased()}.contains(String(word))}}
        
        self.tableView.reloadData()
        
    }
    
    
}


