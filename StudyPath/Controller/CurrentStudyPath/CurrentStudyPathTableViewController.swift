import UIKit

/// The controller of Current Study Path view
class CurrentStudyPathTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var studyPathTableView: UITableView!
    
    /// The model of the current study path
    private var courses: StudyPath = StudyPath.empty(targetHours: 0)
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let username = DataManager.main.loggedIn ?? ""
        let maybeStudyPath = DataManager.main.retrieveStudyPath(username: username)
        courses = maybeStudyPath ?? StudyPath.empty(targetHours: 0)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return courses.allCourses.count
    }

    /// Fill the data of the cells
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let cell = tableView.dequeueReusableCell(withIdentifier: "CurrentStudyPathCourseList", for: indexPath) as? CurrentStudyPathTableViewCell else {return UITableViewCell()}
        // Fill text and info
        cell.courseTitle.textColor = #colorLiteral(red: 0.1298420429, green: 0.1298461258, blue: 0.1298439503, alpha: 1)
        let item = courses.allCoursesWithCompletionInfo[indexPath.row]
        cell.courseTitle.text = item.course.name
        cell.courseHours.text = "\(item.course.hours) hours"
        cell.courseDeliveryMode.text = item.course.contactTeaching ? "contact" : "online"
    
        // Fetch the course image
        let url = item.course.imageUrl.flatMap{URL(string: $0)}
        if let url = url {
            let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
                if let error = error {
                    print("Client error \(error)")
                }
                guard let data = data, error == nil, response != nil else {return}
                do {
                    let img = data
                    DispatchQueue.main.async {
                        cell.courseImg.image = UIImage(data: img)
                    }
                }
            }
            task.resume()
        }
        
        // Change the text color according to the completion status of the course
        if item.completion == .started {
            cell.courseTitle.textColor = #colorLiteral(red: 0.7513522506, green: 0.3490773141, blue: 0.2941740751, alpha: 1)
        }else if item.completion == .completed {
            cell.courseTitle.textColor = #colorLiteral(red: 0.8788378835, green: 0.6719534397, blue: 0.5047522187, alpha: 1)
        }
        
        return cell
    }
    
    
     /// Configure swipe to edit functions: to mark a course as start or complete status, to cancel status marking, or to delete a course from the list
     func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let start = UITableViewRowAction(style: .default, title: "Start") { (action, indexPath) in
            let cell = tableView.cellForRow(at: indexPath) as! CurrentStudyPathTableViewCell
            self.courses = (try? self.courses.startedCourse(at: indexPath.row)) ?? self.courses
            self.saveStudyPath()
            cell.courseTitle.textColor = #colorLiteral(red: 0.7513522506, green: 0.3490773141, blue: 0.2941740751, alpha: 1)
        }
        
        let complete = UITableViewRowAction(style: .default, title: "Complete") { (action, indexPath) in
            let cell = tableView.cellForRow(at: indexPath) as! CurrentStudyPathTableViewCell
            self.courses = (try? self.courses.completedCourse(at: indexPath.row)) ?? self.courses
            self.saveStudyPath()
            cell.courseTitle.textColor = #colorLiteral(red: 0.8788378835, green: 0.6719534397, blue: 0.5047522187, alpha: 1)
        }
        
        let delete = UITableViewRowAction(style: .default, title: "Delete") { (action, indexPath) in
            self.courses = (try? self.courses.withRemoval(at: indexPath.row)) ?? self.courses
            self.saveStudyPath()
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
        
        // Reset status marking
        let reset = UITableViewRowAction(style: .default, title: "Reset") { (action, indexPath) in
            let cell = tableView.cellForRow(at: indexPath) as! CurrentStudyPathTableViewCell
            self.courses = (try? self.courses.updateCompletion(at: indexPath.row, completionStatus: .unknown)) ?? self.courses
            self.saveStudyPath()
            cell.courseTitle.textColor = #colorLiteral(red: 0.1298420429, green: 0.1298461258, blue: 0.1298439503, alpha: 1)
        }
        
        delete.backgroundColor = #colorLiteral(red: 0.9372549057, green: 0.3490196168, blue: 0.1921568662, alpha: 1)
        start.backgroundColor = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
        complete.backgroundColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
        reset.backgroundColor = #colorLiteral(red: 0.9607843161, green: 0.7058823705, blue: 0.200000003, alpha: 1)
        
        // Show the possible actions according to the current state
        if courses.courseStatus(at: indexPath.row) == .started {
            return [delete, reset, complete]
        } else if courses.courseStatus(at: indexPath.row) == .completed {
            return [delete, reset]
        } else {
            return [delete, start]
        }
    }
    
    
    // Override to support conditional editing of the table view.
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .none
    }
    
    func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
            return false
     }

    /// When the user clicks a course, go to the Course Info view and show that course
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let courseSelected = self.courses.course(at: indexPath.row)
        if let course = courseSelected {
            DataManager.main.setViewingCourse(course, canAdd: false)
        }
    }
    
    /// Allow the user to delete the study path, after confirmation
    @IBAction func deleteClicked(_ sender: Any) {
        let alert = UIAlertController(title: "Alert", message: "Are you sure you want to permanently delete this study path?", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: {action in
            self.courses = StudyPath.empty(targetHours: 0)
            self.saveStudyPath()
            self.studyPathTableView.reloadData()
        }))
        
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        
        self.present(alert, animated: true)
    }
    
    /// Save the modifications to the current study path to the database
    private func saveStudyPath(){
        let username = DataManager.main.loggedIn ?? ""
        DataManager.main.saveStudyPath(self.courses, username: username)
    }
    
}
