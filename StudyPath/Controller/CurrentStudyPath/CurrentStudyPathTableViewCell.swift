import UIKit

/// Represents the CurrentCoursePath TableView Cell
class CurrentStudyPathTableViewCell: UITableViewCell {

    /// Tableview cell content: a label with a course title
    @IBOutlet weak var courseTitle: UILabel!
    @IBOutlet weak var courseImg: UIImageView!
    @IBOutlet weak var courseDeliveryMode: UILabel!
    
    @IBOutlet weak var courseHours: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
