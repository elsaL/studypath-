

//
//  GraphTest.swift
//  StudyPathTests
//
//  Created by Andrei Vasilev on 20/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import XCTest
@testable import StudyPath


class GraphTest: XCTestCase {
    
    let emptyGraph = Graph<String>.empty

    func testNodes() {
        let vertexC = Graph<String>.Vertex(data: "C")
        let graph = emptyGraph
            .withVertex(data: "A")
            .withVertex(data: "B")
            .withVertex(vertex: vertexC)
            .withVertex(data: "D")
            .withVertex(data: "E")
        
        
        XCTAssertEqual(graph.vertexCount, 5)
        XCTAssertTrue(graph.contains(data: "A"))
        XCTAssertTrue(graph.contains(data: "B"))
        XCTAssertTrue(graph.contains(data: "C"))
        XCTAssertTrue(graph.contains(data: "D"))
        XCTAssertTrue(graph.contains(data: "E"))
        
        XCTAssertTrue(graph.contains(vertex: vertexC))
        
        XCTAssertTrue(!graph.contains(data: "F"))
        XCTAssertTrue(!graph.contains(data: "G"))
        
    }
    
    func testWithEdges() {
        let vertexA = Graph<String>.Vertex(data: "A")
        let vertexB = Graph<String>.Vertex(data: "B")
        let vertexC = Graph<String>.Vertex(data: "C")
        let vertexD = Graph<String>.Vertex(data: "D")
        let vertexE = Graph<String>.Vertex(data: "E")
        let vertexF = Graph<String>.Vertex(data: "F")
        let vertexG = Graph<String>.Vertex(data: "G")
        
        let graph = emptyGraph
            .withVertex(vertex: vertexA, edgesFrom: [], edgesTo: [vertexB, vertexC])
            .withVertex(vertex: vertexB, edgesFrom: [], edgesTo: [vertexD])
            .withVertex(vertex: vertexC, edgesFrom: [], edgesTo: [vertexE])
            .withVertex(vertex: vertexE, edgesFrom: [vertexD], edgesTo: [])
            .withEdge(from: vertexE, to: vertexF)
            // Add vertex G that will be removed, to test vertex removal
            .withEdge(from: vertexE, to: vertexG)
            .withVertex(vertex: vertexG, edgesFrom: [vertexA, vertexB], edgesTo: [vertexC, vertexD])
            .removedVertex(vertex: vertexG)
        
        let graphWG = graph
            // Remove G by data
            .withEdge(from: vertexE, to: vertexG)
            .withVertex(vertex: vertexG, edgesFrom: [vertexA, vertexB], edgesTo: [vertexC, vertexD])
            .removedVertices(data: "G")

        
        print(graph)
        
        XCTAssert(graph.contains(data: "A"))
        XCTAssert(graph.contains(data: "B"))
        XCTAssert(graph.contains(data: "C"))
        XCTAssert(graph.contains(data: "D"))
        XCTAssert(graph.contains(data: "E"))
        XCTAssert(graph.contains(data: "F"))
        XCTAssert(!graph.contains(data: "G"))
        
        XCTAssert(graphWG.contains(data: "A"))
        XCTAssert(graphWG.contains(data: "B"))
        XCTAssert(graphWG.contains(data: "C"))
        XCTAssert(graphWG.contains(data: "D"))
        XCTAssert(graphWG.contains(data: "E"))
        XCTAssert(graphWG.contains(data: "F"))
        XCTAssert(!graphWG.contains(data: "G"))
        
        XCTAssertEqual(Set(graph.outgoingEdges(vertex: vertexA)), Set([vertexB, vertexC]))
        XCTAssertEqual(Set(graph.outgoingEdges(vertex: vertexB)), Set([vertexD]))
        XCTAssertEqual(Set(graph.outgoingEdges(vertex: vertexC)), Set([vertexE]))
        XCTAssertEqual(Set(graph.outgoingEdges(vertex: vertexD)), Set([vertexE]))
        XCTAssertEqual(Set(graph.outgoingEdges(vertex: vertexE)), Set([vertexF]))
        
        XCTAssertEqual(Set(graph.incomingEdges(vertex: vertexA)), Set([]))
        XCTAssertEqual(Set(graph.incomingEdges(vertex: vertexB)), Set([vertexA]))
        XCTAssertEqual(Set(graph.incomingEdges(vertex: vertexC)), Set([vertexA]))
        XCTAssertEqual(Set(graph.incomingEdges(vertex: vertexD)), Set([vertexB]))
        XCTAssertEqual(Set(graph.incomingEdges(vertex: vertexE)), Set([vertexC, vertexD]))
        XCTAssertEqual(Set(graph.incomingEdges(vertex: vertexF)), Set([vertexE]))
        
        XCTAssertEqual(Set(graphWG.outgoingEdges(vertex: vertexA)), Set([vertexB, vertexC]))
        XCTAssertEqual(Set(graphWG.outgoingEdges(vertex: vertexB)), Set([vertexD]))
        XCTAssertEqual(Set(graphWG.outgoingEdges(vertex: vertexC)), Set([vertexE]))
        XCTAssertEqual(Set(graphWG.outgoingEdges(vertex: vertexD)), Set([vertexE]))
        XCTAssertEqual(Set(graphWG.outgoingEdges(vertex: vertexE)), Set([vertexF]))
        
        XCTAssertEqual(Set(graphWG.incomingEdges(vertex: vertexA)), Set([]))
        XCTAssertEqual(Set(graphWG.incomingEdges(vertex: vertexB)), Set([vertexA]))
        XCTAssertEqual(Set(graphWG.incomingEdges(vertex: vertexC)), Set([vertexA]))
        XCTAssertEqual(Set(graphWG.incomingEdges(vertex: vertexD)), Set([vertexB]))
        XCTAssertEqual(Set(graphWG.incomingEdges(vertex: vertexE)), Set([vertexC, vertexD]))
        XCTAssertEqual(Set(graphWG.incomingEdges(vertex: vertexF)), Set([vertexE]))
        
        // Test for equality
        
        XCTAssert(graph == graphWG)
        
        let graphA = emptyGraph
            .withVertex(vertex: vertexA, edgesFrom: [], edgesTo: [vertexB, vertexC])
            .withVertex(vertex: vertexB, edgesFrom: [], edgesTo: [vertexD])
        
        let graphB = emptyGraph
            .withVertex(vertex: vertexB, edgesFrom: [], edgesTo: [vertexD])
            .withVertex(vertex: vertexA, edgesFrom: [], edgesTo: [vertexB, vertexC])
        
        let graphC = emptyGraph
            .withVertex(data: "B", edgesFrom: [], edgesTo: [vertexD])
            .withVertex(vertex: vertexA, edgesFrom: [], edgesTo: [vertexB, vertexC])
        
        XCTAssert(graphA == graphB)
        XCTAssert(graphB != graphC)
    }
    
    
    func testFind() {
        let vertexA = Graph<String>.Vertex(data: "A")
        let vertexB = Graph<String>.Vertex(data: "B")
        let vertexC = Graph<String>.Vertex(data: "C")
        let vertexD = Graph<String>.Vertex(data: "D")
        let vertexE = Graph<String>.Vertex(data: "E")
        let vertexF = Graph<String>.Vertex(data: "F")
        
        let graph = emptyGraph
            .withVertex(vertex: vertexA, edgesFrom: [], edgesTo: [vertexB, vertexC])
            .withVertex(vertex: vertexB, edgesFrom: [], edgesTo: [vertexD])
            .withVertex(vertex: vertexC, edgesFrom: [], edgesTo: [vertexE])
            .withVertex(vertex: vertexE, edgesFrom: [vertexD], edgesTo: [])
            .withEdge(from: vertexE, to: vertexF)
        
        XCTAssertEqual(graph.findVertices(data: "E"), [vertexE])
        // Check if I can retrieve the incoming edges of vertex E retrieved with findVertices
        XCTAssertEqual(graph.findVertices(data: "E").first.map{Set(graph.incomingEdges(vertex: $0))}, Set([vertexC, vertexD]))
    }


}
