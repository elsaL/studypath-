//
//  ModelCollectionViewCell.swift
//  StudyPath
//
//  Created by Andrei Vasilev on 03/05/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import UIKit

/// The view cell for a model item in Onboarding
class ModelCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var modleTitleLabel: UILabel!
    @IBOutlet weak var unicodeLabel: UILabel!
}
