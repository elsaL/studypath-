//
//  TopicCollectionViewCell.swift
//  StudyPath
//
//  Created by Andrei Vasilev on 03/05/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import UIKit

/// The view cell for a topic item in Onboarding
class TopicCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var topicTitleLabel: UILabel!
}
