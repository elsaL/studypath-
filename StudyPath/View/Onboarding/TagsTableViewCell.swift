//
//  TagsTableViewCell.swift
//  StudyPath
//
//  Created by Andrei Vasilev on 03/05/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import UIKit

/// The view cell for a tag item in Onboarding
class TagsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var tagLabel: UILabel!
    @IBOutlet weak var switchTag: UISwitch!

}
