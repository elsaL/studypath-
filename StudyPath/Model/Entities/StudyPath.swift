import Foundation

/**
 * Represents a study path for the user.
 */
public struct StudyPath {
    
    /// Respresents the list of courses in the study spath and their completion status
    private let coursesWithCompletion: [CourseWithCompletion]
    
    /// How many hours does the user want to study in this study path
    public let targetHours: Int
    
    /// How many hours will be necessary to complete the study path
    public var hours: Int {
        return coursesWithCompletion.reduce(0, {result, item in result + item.course.hours})
    }
    
    /// Is the study path full of courses to reach its target hours
    public var isFull: Bool {
        return hours >= targetHours
    }
    
    /// Did the user complete all the courses in the study path
    public var isCompleted: Bool {
        return !coursesWithCompletion.isEmpty && coursesWithCompletion.allSatisfy{$0.completion == .completed}
    }
    
    /// All the courses the user is currently undertaking
    public var currentCourses: [Course] {
        return coursesWithCompletion.filter{$0.completion == .started}.map{$0.course}
    }
    
    /// All the courses in this path, in insertion order.
    public var allCourses: [Course] {
        return coursesWithCompletion.map{$0.course}
    }
    
    /// All the courses associated with their completion info
    public var allCoursesWithCompletionInfo: [CourseWithCompletion] {
        return coursesWithCompletion
    }
    
    /// Did the user complete all the courses in the study path
    public var completedCourses: [Course] {
        return coursesWithCompletion.filter{$0.completion == .completed}.map{$0.course}
    }
    
    /// Private constructor to create a ModelCourse form its component fields
    private init(courses: [CourseWithCompletion], targetHours: Int) {
        self.coursesWithCompletion = courses
        self.targetHours = targetHours
    }
    
    /**
     * Create an empty study path
     * - Parameter targetHours: the amount of hours the user wants to dedicate to this study path
     */
    public static func empty(targetHours: Int) -> StudyPath {
        return StudyPath(courses: [], targetHours: targetHours)
    }
    
    /**
     * Return this study path with a course added
     * - Parameter course: the course to add to the path
     */
    public func with(course: Course) -> StudyPath {
        return self.with(courses: [course])
    }
    
    /**
     * Return this study path with courses added
     * - Parameter courses: the list of courses to add to the path
     */
    public func with(courses: [Course]) -> StudyPath {
        return StudyPath(courses: coursesWithCompletion +
            // Turn all the Course into CourseWithCompletion
            courses.map({course in CourseWithCompletion(course: course, completion: .unknown)}),
                         targetHours: targetHours)
    }
    
    /**
     * Return this study path with courses added
     * - Parameter coursesWithCompletion: the list of courses to add to the path and their completion status
     */
    public func with(coursesWithCompletion: [CourseWithCompletion]) -> StudyPath {
        return StudyPath(courses: self.coursesWithCompletion + coursesWithCompletion, targetHours: self.targetHours)
    }
    
    /**
     * Return this study path with the specified course started
     * - Parameter at: the index of the specified course
     */
    public func startedCourse(at: Int) throws -> StudyPath {
        return try self.updateCompletion(at: at, completionStatus: .started)
    }
    
    /**
     * Return this study path with the specified course started
     * - Parameter courses: the list of courses to add to the path
     */
    public func startedCourse(course: Course) throws -> StudyPath {
        return try self.updateCompletion(course: course, completionStatus: .started)
    }
    
    /**
     * Return this study path with the specified course completed
     * - Parameter at: the index of the specified course
     */
    public func completedCourse(at: Int) throws -> StudyPath {
        return try self.updateCompletion(at: at, completionStatus: .completed)
    }
    
    /**
     * Return this study path with the specified course completed
     * - Parameter courses: the list of courses to add to the path
     */
    public func completedCourse(course: Course) throws -> StudyPath {
        return try self.updateCompletion(course: course, completionStatus: .completed)
    }
    
    /**
     * Return this study path with a course removed
     * - Parameter at: the index of courses to remove
     */
    public func withRemoval(at: Int) throws -> StudyPath {
        var newCourses = coursesWithCompletion
        
        if (!newCourses.indices.contains(at)) {
            throw StudyPathError.indexOutOfBounds
        }
        
        newCourses.remove(at: at)
        return StudyPath(courses: newCourses, targetHours: targetHours)
    }
    
    /**
     * Return this study path with a course removed
     * If the specified course is not present, return the original study path.
     * - Parameter course: the course to remove
     */
    public func withRemoval(course: Course) throws -> StudyPath {
        if let index = self.indexOf(course: course) {
            return try self.withRemoval(at: index)
        } else {
            throw StudyPathError.elementNotFound
        }
    }
    
    /**
     * Return this study path with a course replaced with another
     * - Parameter at: the position of the course to replace
     * - Parameter course: the replacement course
     */
    public func withReplacement(at: Int, course: Course) throws -> StudyPath {
        return try self.withReplacement(at: at, course: course, completionStatus: .unknown)
    }
    
    /**
     * Return this study path with a course replaced with another.
     * If the specified course is not present, return the original study path.
     * - Parameter oldCourse: the old course
     * - Parameter newCourse: the replacement course
     */
    public func withReplacement(oldCourse: Course, newCourse: Course) throws -> StudyPath {
        return try self.withReplacement(oldCourse: oldCourse, newCourse: newCourse, completionStatus: .unknown)
    }
    
    /**
     * Update the completion status of a course.
     * - Parameter course: the course to updates
     * - Parameter completionStatus: the completion status of the course
     */
    public func updateCompletion(course: Course, completionStatus: CourseStatus) throws -> StudyPath {
        if let index = self.indexOf(course: course) {
            return try self.withReplacement(at: index, course: course, completionStatus: completionStatus)
        } else {
            throw StudyPathError.elementNotFound
        }
    }
    
    /**
     * Update the completion status of a course.
     * - Parameter course: the course to updates
     * - Parameter completionStatus: the completion status of the course
     */
    public func updateCompletion(at: Int, completionStatus: CourseStatus) throws -> StudyPath {
        if (coursesWithCompletion.indices.contains(at)) {
            let cwc = coursesWithCompletion[at]
            return try self.withReplacement(at: at, course: cwc.course, completionStatus: completionStatus)
        } else {
            throw StudyPathError.elementNotFound
        }
    }
    
    /**
     * Return this study path with a course replaced with another and update its completion status.
     * - Parameter oldCourse: the replacement course
     * - Parameter newCourse: the replacement course
     * - Parameter completionStatus: the completion status of the course
     */
    private func withReplacement(oldCourse: Course, newCourse: Course, completionStatus: CourseStatus) throws -> StudyPath {
        if let index = self.indexOf(course: oldCourse) {
            return try self.withReplacement(at: index, course: newCourse, completionStatus: completionStatus)
        } else {
            throw StudyPathError.elementNotFound
        }
    }
    
    /**
     * Return this study path with a course replaced with another and update its completion status.
     * - Parameter at: the index of the course to replace
     * - Parameter course: the replacement course
     * - Parameter completionStatus: the completion status of the course
     */
    public func withReplacement(at: Int, course: Course, completionStatus: CourseStatus) throws -> StudyPath {
        var newCourses = coursesWithCompletion
        
        if (!newCourses.indices.contains(at)) {
            throw StudyPathError.indexOutOfBounds
        }
        
        newCourses.remove(at: at)
        newCourses.insert(CourseWithCompletion(course: course, completion: completionStatus), at: at)
        return StudyPath(courses: newCourses, targetHours: targetHours)
    }
    
    /// Swap two courses in the path given their indices
    public func withSwap(firstAt: Int, secondAt: Int) -> StudyPath {
        let courseA = self.allCoursesWithCompletionInfo[firstAt]
        let courseB = self.allCoursesWithCompletionInfo[secondAt]
        return (try? self
            .withReplacement(at: secondAt, course: courseA.course, completionStatus: courseA.completion)
            .withReplacement(at: firstAt, course: courseB.course, completionStatus: courseB.completion)) ?? self
    }
    
    /// Return a new study path based on the current one but with the specified target hours
    public func withTarget(targetHours: Int) -> StudyPath {
        return StudyPath(courses: coursesWithCompletion, targetHours: targetHours)
    }
    
    /**
     * Return the count of non-completed courses in this path that clash with the specified schedule
     * - Parameter schedule: the specified schedule
     */
    public func clashes(schedule: ReservedSchedule) -> Clashes {
        return Clashes(clashes:
            coursesWithCompletion
                .enumerated()
                .filter{$0.element.completion != .completed}
                .compactMap{
                    if ($0.element.course.schedule.surelyClashes(schedule)) {
                        return Clash(course: $0.element.course, index: $0.offset, surely: true)
                    } else if ($0.element.course.schedule.maybeClashes(schedule)) {
                        return Clash(course: $0.element.course, index: $0.offset, surely: false)
                    } else {
                        return nil
                    }
            }
        )
    }
    
    /**
     * Return the course at this position (starting from 0)
     * - Parameter index: the index of the course
     */
    public func course(at: Int) -> Course? {
        return coursesWithCompletion.indices.contains(at) ?  coursesWithCompletion[at].course : nil
    }
    
    /**
     * Return the completion status of the course at this position (starting from 0)
     * - Parameter index: the index of the course
     */
    public func courseStatus(at: Int) -> CourseStatus? {
        return coursesWithCompletion.indices.contains(at) ?  coursesWithCompletion[at].completion : nil
    }
    
    /**
     * Return the completion status of the course at this position (starting from 0)
     * - Parameter index: the index of the course
     */
    public func courseStatus(course: Course) -> CourseStatus? {
        if let index = self.indexOf(course: course) {
            return self.courseStatus(at: index)
        } else {
            return nil
        }
    }
    
    /**
     * Return true if the specified course is contained in the study path
     * - Parameter index: the index of the course
     */
    public func contains(course: Course) -> Bool {
        return coursesWithCompletion.contains(where: {$0.course == course})
    }
    
    /**
     * Return the index of the specified course in the path
     * - Parameter index: the index of the course
     */
    public func indexOf(course: Course) -> Int? {
        return coursesWithCompletion.firstIndex(where: {$0.course == course})
    }
    
    /// Represents a clash with a course in a study path
    public struct Clash {
        
        /// The clashing course
        public let course: Course
        
        /// The index of the clashing course in the study path
        public let index: Int
        
        /// Is the clash sure or hypotetical
        public let surely: Bool
        
        /// Fileprivate constructor to create a clash
        fileprivate init(course: Course, index: Int, surely: Bool) {
            self.course = course
            self.index = index
            self.surely = surely
        }
    }
    
    /// Represents clashes with courses in a study path
    public struct Clashes {
        
        /// The list of clashes
        public let clashes: [Clash]
        
        /// The amount of sure clashes and possible clashes
        public var clashesCount: ClashesCount {
            return clashes
                .reduce(ClashesCount(), {result, clash in
                    if (clash.surely){
                        return result.withOneMoreSurely()
                    } else {
                        return result.withOneMoreMaybe()
                    }
                })
        }
        
        /// Fileprivate constructor to create a clash
        fileprivate init(clashes: [Clash]) {
            self.clashes = clashes
        }
    }
    
    /// Represents a count of how many courses in this study path clash or might clash with a schedule
    public struct ClashesCount {
        /// How many courses maybe clash with a schedule
        let maybe: Int
        /// How many courses surely clash with a schedule
        let surely: Int
        
        /// Create an empty count
        fileprivate init() {
            self.maybe = 0
            self.surely = 0
        }
        
        /// Private constructor to create a count
        private init(maybe: Int, surely: Int) {
            self.maybe = maybe
            self.surely = surely
        }
        
        /// Return a new count with an additional maybe clash
        func withOneMoreMaybe() -> ClashesCount {
            return ClashesCount(maybe: maybe + 1, surely: surely)
        }
        
        /// Return a new count with an additional sure clash
        func withOneMoreSurely() -> ClashesCount {
            return ClashesCount(maybe: maybe, surely: surely + 1)
        }
    }
    
    /// Represents a course and its completion status
    public struct CourseWithCompletion {
        let course: Course
        let completion: CourseStatus
    }
    
    /// Represents the status of a course
    public enum CourseStatus {
        case unknown, started, completed
    }
    
    /// Represents the errors that can be thrown by a study path
    public enum StudyPathError: Error {
        case indexOutOfBounds
        case elementNotFound
    }
}
