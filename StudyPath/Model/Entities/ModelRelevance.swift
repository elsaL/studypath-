//
//  ModelRelevance.swift
//  StudyPath
//
//  Created by iosdev on 06/05/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import Foundation

/// Represent a relevance relationship between model courses
struct ModelRelevance {
    /// The course that when completed increases the relevance of target
    let completed: ModelCourse
    /// The target of which relevance will be increased when completed is completed
    let target: ModelCourse
    /// The amout of increase in score for this connection
    let score: Double
}
