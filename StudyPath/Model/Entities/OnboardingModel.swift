//
//  OnboardingModel.swift
//  StudyPath
//
//  Created by Andrei Vasilev on 29/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import Foundation

/// The model used during the onboarding process
struct OnboardingModel {
    
    /// The courses available in the course database
    let existingCourses: [Course]
    init(courses: [Course]) {
        self.existingCourses = courses
    }
    

    
    /// Contains list of Topics with scores (exp, preference)
    public var ratedTopicList = [String: (Double, Double)]()
    // The rating of each model: 0 - none, 1 - like, 2 - love, 3 - passion
    public var ratedModelList = [String: Int]()
    // The list of tags that have been picked by the user
    public var ratedTagList  = [String]()
    
    /// Helper to construct relevant tags
    public var relevantTagList = [String]()
    /// Helper to construct model scores
    public var  modelListWithScores = [String: Double]()
    
    /// Populate topics and models from the course database
    public mutating func populateRatedTopicListWithInitialValues(courses: [Course]) {
        var uniqueTopics = Set<String>()
        courses.forEach { course in
            course.model.topics.forEach { topic in
                uniqueTopics.insert(topic)
            }
        }
        uniqueTopics.forEach { uniqueItem in
            ratedTopicList.updateValue((1,1), forKey: uniqueItem)
        }
    }
    
    /// Update the rating and experience of topics
    public mutating func updateRatedTopicList(key: String, exp: Double, pref: Double ) {
        ratedTopicList.updateValue((exp, pref), forKey: key)
    }
    
    /// Create the scored models after the user has picked their preferences for topics
    public mutating func createScoredModelList() {
        // Find all the models from the course database
        let existingModels = existingCourses.map({ $0.model})
        // Rate each model from its topics
        ratedTopicList.forEach { ratedTopic in
            existingModels.forEach { model in
                if model.topics.contains(ratedTopic.key) {
                    // The rating increases with preference and decreases with difference between user experience and model level
                    let modelScore = ratedTopic.value.1 - ((Double(model.level) <= (ratedTopic.value.0 * 1.5)) ? (ratedTopic.value.0 * 1.5) - Double(model.level) : 0.0 )
                    
                    if self.ratedModelList.keys.contains(model.name) {
                        guard let previousValue = modelListWithScores[model.name] else { return }
                        // Average the new result with the previous one
                        // NOTE: This should be changed to a running average or similar function, but its relevance is minimal on the result
                        let newValue = (previousValue + modelScore) / 2
                        modelListWithScores.updateValue(newValue, forKey: model.name)
                    } else {
                        modelListWithScores.updateValue(modelScore, forKey: model.name)
                    }
                }
            }
        }
    }
    
    /// Add or remove values to ratedModelList, which represents user preference like love passion
    public mutating func populateRatedModelList(title: String, grade: Int) {
        if grade == 0 && ratedTopicList[title] != nil {
            self.ratedModelList.removeValue(forKey: title)
        } else {
            self.ratedModelList.updateValue(grade, forKey: title)
        }
    }
    
    
    /// Create relevant tag list (tags relevant to user chosen models)
    public mutating func pupulateRelevantTagList() {
        let sortedModelList = ratedModelList.sorted(by: { $0.value > $1.value})
        sortedModelList.forEach { (key, value) in
            existingCourses.forEach { course in
                if course.model.name == key {
                    course.tags.forEach { tag in
                        if (!relevantTagList.contains(tag)) {
                            relevantTagList.append(tag)
                        }
                    }
                }
            }
        }
    }
    
    /// Create ratedTagList (tags that the user has chosen)
    public mutating func populateRatedTagList(tag: String) {
        if ratedTagList.contains(tag) {
            ratedTagList.removeAll(where: {$0 == tag})
        } else {
            ratedTagList.append(tag)
        }
    }
}

