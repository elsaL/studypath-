//
//  WeeklySchedule.swift
//  StudyPath
//
//  Created by iosdev on 22/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import Foundation

/**
 * A weekly schedule capable to represent booking for morning,
 * afternoon and evening for each week day.
 */
public struct WeeklySchedule: Equatable {
    /// Holds the set of the information for each time slot
    private let bookings: BitSet<Int32>
    
    /// Represents an empty weekly schedule (all time slots are free).
    static var empty : WeeklySchedule {
        return WeeklySchedule(BitSet.empty)
    }
    
    /// Private constructor to create a WeeklySchedule from bookings data
    private init (_ bookings: BitSet<Int32>) {
        self.bookings = bookings
    }
    
    /// Return the index of a time slot given ita day and time of the day
    private func dayAndTimeToBitValue(day: Day, time: TimeSlot) -> Int32 {
        return day.rawValue * Int32(TimeSlot.allCases.count) + time.rawValue
    }
    
    /**
     * Return a weekly schedule formed by merging this weekly schedule with another.
     * - Parameter other: the other weekly schedule
     */
    func merging(_ other: WeeklySchedule) -> WeeklySchedule {
        return WeeklySchedule(self.bookings + other.bookings)
    }
    
    /**
     * Return a weekly schedule formed by intersecting this weekly schedule with another.
     * The intersection of an unknown and a known time slot in the same day is the known time slot.
     * - Parameter other: the other weekly schedule
     */
    func intersection(_ other: WeeklySchedule) -> WeeklySchedule {
        let days = Day.allCases
        let union = self.merging(other)
        // Make it so that intersecting an unknown time slot and a known one will return the known one
        let unknownCorrection = days.reduce(BitSet<Int32>.empty, {result, day in
            let booking = union.getBookingsFor(day: day)
            // There is an unknown plus at least a known slot in the union
            if (booking.count > 1 && booking.contains(TimeSlot.unknown)){
                let knownTimeSlots = booking.filter{$0 != TimeSlot.unknown}
                    .map{dayAndTimeToBitValue(day: day, time: $0)}
                let knownSlotsAsBitSet = try? BitSet.fromArray(knownTimeSlots)
                return result + (knownSlotsAsBitSet ?? BitSet.empty)
            } else {
                return result
            }
        })
        return WeeklySchedule(self.bookings.intersection(other.bookings) + unknownCorrection)
    }
    
    /**
     * Return a this weekly schedule with an additional booking on the specified day and time.
     * - Parameter day: the day of the new booking
     * - Parameter time: the time slot of the new booking
     */
    func withBookingOn(day: Day, time: TimeSlot) -> WeeklySchedule {
        let newBooking = try? BitSet.singleton(dayAndTimeToBitValue(day: day, time: time))
        return WeeklySchedule(self.bookings + (newBooking ?? BitSet.empty))
    }
    
    /**
     * Return a this weekly schedule with additional bookings on the specified day.
     * - Parameter day: the day of the new booking
     * - Parameter times: the time slots of the new booking during the day
     */
    func withBookingsOn(day: Day, times: [TimeSlot]) -> WeeklySchedule {
        let scheduleAdditions = times.reduce(WeeklySchedule.empty,
                                             {result, time in result.withBookingOn(day: day, time: time)})
        return self + scheduleAdditions
    }
    
    /**
     * Return a this weekly schedule with an additional booking schedule for each specified day.
     * - Parameter days: the days of the added schedule
     * - Parameter times: the time slots to be booked in the specified days
     */
    func withSameBookingsOn(days: [Day], times: [TimeSlot]) -> WeeklySchedule {
        let scheduleAdditions = days.reduce(WeeklySchedule.empty,
                                            {result, day in result.withBookingsOn(day: day, times: times)})
        return self + scheduleAdditions
    }
    
    /**
     * Return a this weekly schedule with an additional booking schedule for each working day.
     * - Parameter times: the time slots to be booked every working day
     */
    func withSameBookingsEveryWorkingDay(times: [TimeSlot]) -> WeeklySchedule {
        let days = [Day.monday, Day.tuesday, Day.wednesday, Day.thursday, Day.friday]
        return self.withSameBookingsOn(days: days, times: times)
    }
    
    /**
     * Return a this weekly schedule with an additional booking schedule for each week day.
     * - Parameter times: the time slots to be booked every week day
     */
    func withSameBookingsEveryDay(times: [TimeSlot]) -> WeeklySchedule {
        let days = Day.allCases
        return self.withSameBookingsOn(days: days, times: times)
    }
    
    /**
     * Return the complement of this weekly schedule
     */
    var complement: WeeklySchedule {
        let days = Day.allCases
        let knownTimes = [TimeSlot.morning, TimeSlot.afternoon, TimeSlot.evening]
        let byDay = days.map{day in (day, self.getBookingsFor(day: day))}
        let complement = byDay.map{pair in (pair.0, knownTimes.filter{!pair.1.contains($0)})}
        return complement.reduce(WeeklySchedule.empty, {result, value in
            result.withBookingsOn(day: value.0, times: value.1)
        })
    }
    
    /**
     * Return the time slots booked in a specific day of the week.
     * - Parameter day: the specified day
     */
    func getBookingsFor(day: Day) -> [TimeSlot] {
        let bookedTimes = TimeSlot.allCases.compactMap{time in
            bookings.contains(dayAndTimeToBitValue(day: day, time: time)) ? time : nil
        }
        return bookedTimes
    }
    
    /**
     * Check if a specific time slots may be available.
     * - Parameter day: the day of the time slot
     * - Parameter time: the time slot in the specified day
     */
    func checkMaybeAvailable(day: Day, time: TimeSlot) -> Bool {
        let value = dayAndTimeToBitValue(day: day, time: time)
        
        let bookedTimes = TimeSlot.allCases.map{time in
            bookings.contains(dayAndTimeToBitValue(day: day, time: time))
        }
        
        /*
         * There might a generic free time slot if there are at least two booked time slots
         * less than the possible bookings, including unknown.
         */
        let canBeAvailable = bookedTimes.filter{$0}.count <= bookedTimes.count - 2
        
        // We are checking if an unknown time slot is available
        if (time == TimeSlot.unknown) {
            return canBeAvailable
        } else {
            // A known time slot can be available only if is not contained in the bookings
            return !bookings.contains(value) && canBeAvailable
        }
        
    }
    
    /**
     * Check if a specific time slots is surely available.
     * - Parameter day: the day of the time slot
     * - Parameter time: the time slot in the specified day
     */
    func checkSurelyAvailable(day: Day, time: TimeSlot) -> Bool {
        let value = dayAndTimeToBitValue(day: day, time: time)
        let unknown = dayAndTimeToBitValue(day: day, time: TimeSlot.unknown)
        
        let ifUnknownNoBookingAllowed = bookings.count > 0 && time == .unknown
        
        /*
         * To be sure the time is free, we need to check if there is not booking at an unknown time
         * and if there is no known booking on the specified time slot. If the available time asked
         * for is unknown, no time can be booked in the day.
         */
        return !bookings.contains(unknown) && (!bookings.contains(value) || ifUnknownNoBookingAllowed)
    }
    
    /**
     * Check if another weekly schedule could clash with this one
     * - Parameter with: the other weekly schedule
     */
    func maybeClashes(_ other: WeeklySchedule) -> Bool {
        return !self.intersection(other).isEmpty()
    }
    
    /**
     * Check if another weekly schedule clashes for sure with this one
     * - Parameter with: the other weekly schedule
     */
    func surelyClashes(_ other: WeeklySchedule) -> Bool {
        let timeSlotsNum = TimeSlot.allCases.count
        
        // If only unknown time slots clash, then there might not be a clash
        return !bookings.intersection(other.bookings).toIntSet().allSatisfy{value in
            value % timeSlotsNum == TimeSlot.unknown.rawValue}
    }
    
    /**
     * Check if a specific time slots is booked.
     * - Parameter day: the day of the time slot
     * - Parameter time: the time slot in the specified day
     */
    func checkBooked(day: Day, time: TimeSlot) -> Bool {
        let value = dayAndTimeToBitValue(day: day, time: time)
        return bookings.contains(value)
    }
    
    func isEmpty() -> Bool {
        return bookings.count == 0
    }
    
    /**
     * Merge to weekly schedules into a new one.
     * - Parameter this: the first schedule
     * - Parameter other: the second schedule
     */
    static func + (this: WeeklySchedule, other: WeeklySchedule) -> WeeklySchedule {
        return this.merging(other)
    }
    
    /**
     * The time slots available in a day. An "unknown" booking could be located at any time of the day.
     */
    enum TimeSlot: Int32, CaseIterable {
        case unknown = 0, morning = 1, afternoon = 2, evening = 3
    }
    
    /**
     * The days of the week.
     */
    enum Day: Int32, CaseIterable {
        case monday = 0, tuesday = 1, wednesday = 2, thursday = 3, friday = 4, saturday = 5, sunday = 6
    }
}


