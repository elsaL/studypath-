
import Foundation
import CommonCrypto

/**
 Represent data about a user
 */
public class UserInfo: NSObject, NSCoding{
    
    /**
     Username of the user
     */
    public let username:String
    
    /**
     Email of the user
     */
    public let email:String
    
    /**
     Full name(optional) of the user
     */
    public let fullName:String?
    
    /**
     Hash of the password the user has chosen
     */
    public let passwordHash:String
    
    /**
     Create a user with username, email, fullName(optional), passwordHash
     */
    init(username:String, email:String, fullName:String?, passwordHash:String) {
        self.username = username
        self.email = email
        self.fullName = fullName
        self.passwordHash = passwordHash
    }
    
    
    init(username:String, email:String, fullName:String?, password:String) {
        self.username = username
        self.email = email
        self.fullName = fullName
        self.passwordHash = UserInfo.computePwdHash(password:password)
    }
    
    /**
     Compute the hash of a given password
     */
    private static func computePwdHash(password:String) -> String{
        let data = Data(password.utf8)
        var digest = [UInt8](repeating: 0, count:Int(CC_SHA1_DIGEST_LENGTH))
        data.withUnsafeBytes {
            _ = CC_SHA1($0, CC_LONG(data.count), &digest)
        }
        let hexBytes = digest.map { String(format: "%02hhx", $0) }
        return hexBytes.joined()
    }
    
    
    /**
     Check if the given password matches with the password chosen by the user
     - parameter password: check if this password matches with the one given by the user
     */
    public func verifyPassword(password:String)->Bool{
        return UserInfo.computePwdHash(password: password) == self.passwordHash
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(username, forKey: "username")
        aCoder.encode(email, forKey: "email")
        aCoder.encode(fullName, forKey: "fullName")
        aCoder.encode(passwordHash, forKey: "passwordHash")
    }
    
    public required convenience init?(coder aDecoder: NSCoder) {
        let username = aDecoder.decodeObject(forKey: "username") as! String
        let email = aDecoder.decodeObject(forKey: "email") as! String
        let fullName = aDecoder.decodeObject(forKey: "fullName") as! String?
        let passwordHash = aDecoder.decodeObject(forKey: "passwordHash") as! String
        
        self.init(username: username, email: email, fullName: fullName, passwordHash: passwordHash)
    }
}
