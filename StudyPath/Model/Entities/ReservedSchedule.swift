//
//  ReservedSchedule.swift
//  StudyPath
//
//  Created by iosdev on 22/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import Foundation

/// Represents a yearly or multi-yearly schedule
public struct ReservedSchedule: Equatable {
    
    /// Mapping from each year to the schedule of each of its week
    private let years: [Int:[WeeklySchedule]]
    
    /// Create an empty schedule
    static var empty: ReservedSchedule {
        return ReservedSchedule(years: [Int:[WeeklySchedule]]())
    }
    
    /// Private constructor to create a schedule given an already existing internal representation.
    private init (years: [Int:Array<WeeklySchedule>]){
        self.years = years
    }
    
    /**
     * Return a schedule containing only the selected year, but mapped to a different year
     * - Parameter fromYear: the year to pick
     * - Parameter toYear: the year to be mapped to
     */
    public func pickYear(fromYear: Int, toYear: Int) -> ReservedSchedule{
        return years[fromYear].map{ReservedSchedule(years: [toYear:$0])} ?? ReservedSchedule.empty
    }
    
    /**
     * Return an Array containing a weekly schedule for each week of the year given a single
     * weekly schedule and the indices of the weeks it applies to
     */
    private func weeksFromIndicesAndSchedule(weeks: [Int], weeklySchedule: WeeklySchedule) -> [WeeklySchedule]{
        let weeksInAYear = 52
        return (0..<weeksInAYear).map{week in weeks.contains(week) ? weeklySchedule : WeeklySchedule.empty}
    }
    
    /**
     * Return this schedule with additional schedule for the specified weeks in the specified year.
     * - Parameter year: the specified year
     * - Parameter weeks: the specified weeks in the specified year
     * - Parameter weeklySchedule: the schedule that will additionally be booked in each week
     */
    public func withReserved(year: Int, weeks: [Int], weeklySchedule: WeeklySchedule) -> ReservedSchedule{
        let newWeeks = weeksFromIndicesAndSchedule(weeks: weeks, weeklySchedule: weeklySchedule)
        
        // If the year already exists, merge all the weekly shedules for that year...
        let scheduleModification = years[year].map { oldWeeks in
            oldWeeks.zipWith(newWeeks, combine: {oldWeeklySchedule, newWeeklySchedule in
                oldWeeklySchedule + newWeeklySchedule})
        } ?? newWeeks // ...otherwise just take the new yearly schedule as it is
        
        // Return the schedule with the modified year
        return ReservedSchedule(years: self.years.merging([year:scheduleModification], uniquingKeysWith: {$1}))
    }
    
    /**
     * Return a schedule formed by this schedule merged with another schedule.
     * - Parameter other: the other schedule
     */
    public func merging(_ other: ReservedSchedule) -> ReservedSchedule {
        let yearsA = years
        let yearsB = other.years
        
        let mergedSchedules = yearsA.merging(yearsB, uniquingKeysWith:
        {scheduleA, scheduleB in scheduleA.zipWith(scheduleB,
                                                   combine: {weekA, weekB in weekA + weekB})})
        
        return ReservedSchedule(years: mergedSchedules)
    }
    
    /**
     * Return a schedule formed by this schedule intersected with another schedule.
     * - Parameter other: the other schedule
     */
    public func intersection(_ other: ReservedSchedule) -> ReservedSchedule {
        // Find all the years common to the two schedules
        let commonYears = years.keys.filter{other.years[$0] != nil}
        let commonYearSchedulesA = commonYears.compactMap{years[$0]}
        let commonYearSchedulesB = commonYears.compactMap{other.years[$0]}
        
        // Intersect every week in the common years
        let joinedSchedules = commonYearSchedulesA
            .zipWith(commonYearSchedulesB,
                     combine: {weeksA, weeksB in
                        weeksA.zipWith(weeksB, combine: {weekA, weekB in
                            weekA.intersection(weekB)})})
        
        let newSchedule = commonYears
            // Create year-schedule pairs
            .zipWith(joinedSchedules, combine: {year, weeklySchedules in (year, weeklySchedules)})
            // Turn the pairs into a dictionary
            .reduce(into: [:]) { $0[$1.0] = $1.1 }
        
        return ReservedSchedule(years: newSchedule)
    }
    
    public func isEmpty() -> Bool {
        return years.values.allSatisfy {weeks in weeks.allSatisfy{week in week.isEmpty()}}
    }
    
    /**
     * Return true if the two schedules are surely compatible
     * - Parameter other: the other schedule
     */
    public func surelyCompatible(_ other: ReservedSchedule) -> Bool {
        return self.intersection(other).isEmpty()
    }
    
    /**
     * Return true if the two schedules clash for sure
     * - Parameter other: the other schedule
     */
    public func surelyClashes(_ other: ReservedSchedule) -> Bool {
        //Math Note: not forAll (not surelyClashes) == existsOne (surelyClashes)
        return !forAllInCommonYears(other, predicate: {(weekA, weekB) in !weekA.surelyClashes(weekB)})
    }
    
    /**
     * Return true if the two schedules might be clashing
     * - Parameter other: the other schedule
     */
    public func maybeClashes(_ other: ReservedSchedule) -> Bool {
        return !surelyCompatible(other)
    }
    
    /**
     * Return the index of the first booked week of the schedule
     */
    public func startingWeek() -> Int? {
        let firstYear = years.keys.sorted().first
        return firstYear
            .flatMap{year in years[year]}
            .flatMap{weeks in weeks.firstIndex(where: {!$0.isEmpty()})}
    }
    
    /**
     * Return true if the given predicate is true for all the weeks in the common years
     * between this schedule and the other schedule.
     * - Parameter other: the other schedule
     * - Parameter predicate: the given predicate
     */
    private func forAllInCommonYears(_ other: ReservedSchedule,
                                        predicate: (WeeklySchedule, WeeklySchedule) -> Bool)
        -> Bool {
            let commonYears = years.keys.filter{other.years[$0] != nil}
            let commonYearSchedulesA = commonYears.compactMap{years[$0]}
            let commonYearSchedulesB = commonYears.compactMap{other.years[$0]}
            
            // Test the predicate for every week in the common years
            let predicateTest = commonYearSchedulesA
                .zipWith(commonYearSchedulesB,
                         combine: {weeksA, weeksB in
                            weeksA.zipWith(weeksB, combine: {weekA, weekB in
                                predicate(weekA, weekB)})})
            
            return predicateTest.flatMap{$0}.allSatisfy{$0}
    }
}
