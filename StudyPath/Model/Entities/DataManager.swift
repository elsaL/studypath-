//
//  DataManager.swift
//  StudyPath
//
//  Created by iosdev on 04/05/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import Foundation
import UIKit
import CoreData

/**
 * Represents a single interface to store and retrieve the application data,
 * including user info and preference, login and course database.
 */
class DataManager {
    
    /// The courses database
    private let coursesJSON: String
    /// The model courses database
    private let modelJSON: String
    /// The relevance database
    private let relevanceJSON: String
    
    /// The course the user is currently viewing
    private var viewingCourse: Course?
    /// Can the user add the viewing course to their study path?
    private var viewingCourseCanAdd: Bool = true
    /// Store the loaded courses
    private var courseCache: [Course] = []
    /// Store the loaded model courses
    private var modelCache: [ModelCourse] = []
    /// Store the increased relevance of model courses when one model course is completed
    private var relevanceCache: [ModelRelevance] = []
    /// Store the dependency graph of the model courses
    private var modelGraphCache: Graph<ModelCourse> = Graph.empty
    
    /// The main DataManager for the application
    public static let main = DataManager(
        coursesJSON: "SampleCourses",
        modelJSON: "SampleModelCourse",
        relevanceJSON: "RelevanceTable"
    )
    
    /// The course that will be shown in Course Info View
    public var currentlyViewingCourse: Course? {
        return viewingCourse
    }
    
    /// Can the user add the viewing course to their study path?
    public var currentlyViewingCourseCanAdd: Bool {
        return viewingCourseCanAdd
    }
    
    /// All the courses in the database
    public var courses: [Course] {
        loadCourseData()
        return courseCache
    }
    
    /// All the model courses in the database
    public var models: [ModelCourse] {
        loadCourseData()
        return modelCache
    }
    
    /// The dependency graph of the model courses in the database
    public var modelGraph: Graph<ModelCourse> {
        loadCourseData()
        return modelGraphCache
    }
    
    /// Table containing the increase in score that completing a model causes to other models
    public var relevanceRelationships: [ModelRelevance] {
        return relevanceCache
    }
    
    /// The logged in username. Nil if the user is not logged in
    public var loggedIn: String? {
        let logins = fetchRequest(of: LoginData.self)
        return logins.first?.login
    }
    
    /// Load the courses, model courses and model relevances from the json database
    private func loadCourseData(){
        // Check if at least one of the caches is empty, otherwise do not load anything
        if (!(courseCache.isEmpty && modelCache.isEmpty && relevanceCache.isEmpty && modelGraphCache == Graph.empty)){
            return
        }
        
        // Make sure all the caches are empty
        courseCache = []
        modelCache = []
        relevanceCache = []
        modelGraphCache = Graph.empty
        
        // Load the json tables from the database
        let mss = loadJSON(modelJSON, ofType: ([CourseModelSerializedState]).self)
        let css = loadJSON(coursesJSON, ofType: ([CourseSerializedState]).self)
        let rss = loadJSON(relevanceJSON, ofType: ([RelevanceSerializedState]).self)
        
        // Create the model courses from the database
        modelCache = mss.map{ModelCourse(id: $0.mID, name: $0.name, level: $0.level, topics: $0.topics)}
        
        // Create the courses from the database
        courseCache = css.compactMap{c in
            modelCache.filter{m in m.id == c.model}.first.map{model in
                Course(
                    name: c.title,
                    description: c.description,
                    provider: c.provider,
                    contactTeaching: c.contactTeaching,
                    language: c.language,
                    location: c.location,
                    model: modelCache.filter{m in m.id == c.model}.first!,
                    schedule: c.schedule.reduce(ReservedSchedule.empty, {
                        $0.withReserved(
                            year: 0,
                            weeks: [Int]($1.startWeek ..< $1.endWeek + 1),
                            weeklySchedule: constructWeeklySchedule(schedule: $1))}),
                    tags: c.tags,
                    hours: c.hours,
                    imageUrl: c.image,
                    url: c.url
                )
            }
        }
        
        // Add all the vertices to the model course dependency graph
        modelGraphCache = modelCache.reduce(Graph.empty, {result, vertex in result.withVertex(data: vertex)})
        
        // Add all the edges between the vertices of the model course dependency graph
        modelGraphCache = mss.reduce(modelGraph, {result, vertex in
            let targetVertex = result.findVertices{$0.id == modelCache.filter{$0.id == vertex.mID}.first?.id}
            let sources = modelCache.filter{vertex.prerequisites.contains($0.id)}
            let sourceVertices = result.findVertices{data in sources.contains(where: {$0.id == data.id})}
            
            if let targetVertex = targetVertex.first {
                return sourceVertices.reduce(result, {partial, node in
                    partial.withEdge(from: node, to: targetVertex)})
            } else {
                return result
            }
            
        })
        
        // Create the model relevance table from the database data
        relevanceCache = rss.flatMap{relevance -> [ModelRelevance] in
            // The model that when completed will give a score bonus to other models
            let completed = modelCache.filter{$0.id == relevance.completed}.first
            // The targets of the bonus
            let targets = relevance.targets.compactMap{entry -> (ModelCourse, Double)? in
                let maybeTarget = modelCache.filter{$0.id == entry.key}.first
                if let target = maybeTarget {
                    // The bonus
                    let score = entry.value
                    return (target, score)
                    
                } else {
                    return nil
                }
            }
            if let completed = completed {
                return targets.map{ModelRelevance(completed: completed, target: $0.0, score: $0.1)}
            } else {
                return []
            }
        }
    }
    
    /// Create the weekly schedule of a course from json data
    private func constructWeeklySchedule(schedule: ScheduleSerializedState) -> WeeklySchedule{
        var result = WeeklySchedule.empty
        // Map from the timeslot saved in json to their model's equivalent
        let timeMap: [String: WeeklySchedule.TimeSlot] = ["M" : .morning, "A": .afternoon, "E": .evening, "U": .unknown]
        // Add in the schedule the specified timeslots for each day
        result = result.withSameBookingsEveryWorkingDay(times: schedule.workDay?.compactMap{timeMap[$0]} ?? [])
            .withBookingsOn(day: .monday, times: schedule.monday?.compactMap{timeMap[$0]} ?? [])
            .withBookingsOn(day: .tuesday, times: schedule.tuesday?.compactMap{timeMap[$0]} ?? [])
            .withBookingsOn(day: .wednesday, times: schedule.wednesday?.compactMap{timeMap[$0]} ?? [])
            .withBookingsOn(day: .thursday, times: schedule.thursday?.compactMap{timeMap[$0]} ?? [])
            .withBookingsOn(day: .friday, times: schedule.friday?.compactMap{timeMap[$0]} ?? [])
            .withBookingsOn(day: .saturday, times: schedule.saturday?.compactMap{timeMap[$0]} ?? [])
            .withBookingsOn(day: .sunday, times: schedule.sunday?.compactMap{timeMap[$0]} ?? [])
        
        return result
    }
    
    /**
     * Create the DataManage by specifying the sources of json data
     */
    public init(coursesJSON: String, modelJSON: String, relevanceJSON: String) {
        self.coursesJSON = coursesJSON
        self.modelJSON = modelJSON
        self.relevanceJSON = relevanceJSON
        self.modelGraphCache = Graph.empty
    }
    
    /**
     * Return the list of courses that receive a score bonus from this course being completed,
     * and the amount of the bonus.
     */
    public func relevanceRelationship(completed: ModelCourse) -> [ModelRelevance] {
        loadCourseData()
        return relevanceRelationships.filter{$0.completed.id == completed.id}
    }
    
    /// Set which course should be shown in Course Info View
    public func setViewingCourse(_ course: Course) {
        viewingCourse = course
        viewingCourseCanAdd = true
    }
    
    /**
     * Set which course should be shown in Course Info View and specify
     * if it can be added to the user's study path.
     */
    public func setViewingCourse(_ course: Course, canAdd: Bool) {
        viewingCourse = course
        viewingCourseCanAdd = canAdd
    }

    ///Delete a user from Core Data
    public func deleteUser(username:String){
        var users: [UserData] = []
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest : NSFetchRequest = UserData.fetchRequest()
        fetchRequest.predicate = nil
        
        do {
            users = try managedContext.fetch(fetchRequest)
            if let toBeDeleted = (users.filter{$0.user?.username == username}.first) {
                managedContext.delete(toBeDeleted)
            }
        } catch {}
    }
    
    /**
     Do login given a username, if the username exists.
    */
    public func login(username: String) {
        let user = fetchUsers().filter{$0.username == username}.first
        let managedObjectContext = AppDelegate.viewContext
        let logins = fetchRequest(of: LoginData.self)
        logins.forEach{managedObjectContext.delete($0)}
        
        if let user = user {
            managedObjectContext.performAndWait{
                let data = LoginData(context: managedObjectContext)
                data.login = user.username
                (UIApplication.shared.delegate as! AppDelegate).saveContext()
            }
        }
    }
    
    /**
     Do logout the currently logged in user.
     */
    public func logout() {
        let managedObjectContext = AppDelegate.viewContext
        let logins = fetchRequest(of: LoginData.self)
        logins.forEach{managedObjectContext.delete($0)}
        managedObjectContext.performAndWait{
            (UIApplication.shared.delegate as! AppDelegate).saveContext()
        }
    }
    
    /**
     Fetch all the users from Core Data
     */
    public func fetchUsers()->[UserInfo]{
        return fetchRequest(of: UserData.self).compactMap{$0.user}
    }
    
    /**
     Fetch user from core data with given username
     - parameter username: fetch the user with the given username from core data
     - return: the found user or nil in case there is no user with such username
     */
    public func fetchUserByUsername(username:String)->UserInfo?{
        return fetchUsers().filter{$0.username == username}.first
    }
    
    /**
     Take validated user input and create an instance of UserInfo
     Save newly created User to core data
     - parameter username:
     - parameter email:
     - parameter password:
     - parameter fullname: not used in UI, to be deleted?)
     */
    public func saveUser(_ username: String, _ email: String, _ password: String) {
        let userInfo = UserInfo(username: username, email: email, fullName: nil, password: password)
        
        let managedObjectContext = AppDelegate.viewContext
        
        managedObjectContext.performAndWait{
            let data = UserData(context: managedObjectContext)
            data.user = userInfo
            (UIApplication.shared.delegate as! AppDelegate).saveContext()
        }
    }
    
    /**
     Save the user's study path in the Core Data
     - parameter studyPath: the study path
     - parameter username: the username of the user
     */
    public func saveStudyPath(_ studyPath: StudyPath, username: String) {
        let managedObjectContext = AppDelegate.viewContext
        
        let paths = fetchRequest(of: StudyPathData.self)
        paths.filter{$0.user == username}.forEach{managedObjectContext.delete($0)}
        
        let studyPathSerializedState = StudyPathSerializedState(items:
            studyPath.allCoursesWithCompletionInfo.map{StudyPathItemSerializedState(
                courseName: $0.course.name,
                courseDescription: $0.course.description,
                courseProvider: $0.course.provider,
                status: $0.completion == .started ? "started" :
                    ($0.completion == .completed ? "completed" : "unknown")
            )}
        )
        
        managedObjectContext.performAndWait{
            let data = StudyPathData(context: managedObjectContext)
            data.user = username
            data.studyPath = studyPathSerializedState
            data.targetHours = Int32(studyPath.targetHours)
            (UIApplication.shared.delegate as! AppDelegate).saveContext()
        }
    }
    
    /**
     Fetch the study path of the current user.
     - parameter username: the username
     */
    public func retrieveStudyPath(username: String) -> StudyPath? {
        let completions: [String: StudyPath.CourseStatus] = ["unknown": .unknown, "started": .started, "completed": .completed]
        loadCourseData()
        return fetchUserByUsername(username: username).flatMap {user in
            let data: [StudyPathData] = fetchRequest(of: StudyPathData.self)
            return data.filter{$0.user == user.username}.first.flatMap{entry in
                entry.studyPath.flatMap {studyPathSerializedState in
                    let result = StudyPath.empty(targetHours: Int(entry.targetHours))
                    // Add all the courses that have the same name, decription and provider as the saved courses
                    let coursesWithCompletion = studyPathSerializedState.items.compactMap {item -> StudyPath.CourseWithCompletion? in
                        let completion: StudyPath.CourseStatus = completions[item.status] ?? .unknown
                        let course = courseCache.filter{$0.name == item.courseName && $0.description == item.courseDescription && $0.provider == item.courseProvider}.first
                        
                        return course.flatMap{StudyPath.CourseWithCompletion(course: $0, completion: completion)}
                    }
                    return result.with(coursesWithCompletion: coursesWithCompletion)
                }
            }
        }
    }
    
    /**
     Take validated user input and create an instance of UserInfo
     Save newly created User to core data
     - parameter username:
     - parameter email:
     - parameter password:
     - parameter fullname: not used in UI, to be deleted?)
     */
    public func saveUserPreferences(_ preferences: UserPreferences) {
        let managedObjectContext = AppDelegate.viewContext
        
        // Delete old preferences for this user
        let prefs = fetchRequest(of: UserPreferencesData.self)
        prefs.filter{$0.preferences?.username == preferences.username}.forEach{managedObjectContext.delete($0)}
        
        managedObjectContext.performAndWait{
            let data = UserPreferencesData(context: managedObjectContext)
            data.preferences = preferences
            (UIApplication.shared.delegate as! AppDelegate).saveContext()
        }
    }
    
    /**
     Take validated user input and create an instance of UserInfo
     Save newly created User to core data
     - parameter username: the username
     */
    public func retrieveUserPreferences(username: String) -> UserPreferences? {
        return fetchUserByUsername(username: username).flatMap {user in
            let data: [UserPreferencesData] = fetchRequest(of: UserPreferencesData.self)
            return data.filter{$0.preferences?.username == username}.first?.preferences
        }
    }
    
    /// Fetch all the managed objects of a certain type from Core Data
    private func fetchRequest<T: NSManagedObject>(of type:T.Type)->[T]{
        var results: [T] = []
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest : NSFetchRequest = type.fetchRequest()
        fetchRequest.predicate = nil
        do {
            results = (try managedContext.fetch(fetchRequest)) as! [T]
        } catch {
            return []
        }
        return results
    }
    
    /// Load a json object from a file
    private func loadJSON<T: Decodable>(_ file: String, ofType type: T.Type) -> T {
        if let path = Bundle.main.path(forResource: file, ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                return try! JSONDecoder().decode(T.self, from: data)
            } catch {
                fatalError("Internal Error: Malformed database.")
            }
        } else {
            fatalError("Internal Error: Database file not found.")
        }
    }
    
    // The json parsed representation of a course
    public struct CourseSerializedState: Decodable {
        /// The unique id of the course
        public let id: String
        /// The name of the course
        public let title: String
        /// The description of the course
        public let description: String
        /// The provider of the course (istitution or website)
        public let provider: String
        /// Does the course have contact lessons?
        public let contactTeaching: Bool
        /// The language or languages of education for this course
        public let language: [String]
        /// The city where the course is held or available (blanck for online courses)
        public let location: String
        /// The model course this course is linked to
        public let model: String
        /// The course reserved schedule
        public let schedule: [ScheduleSerializedState]
        /// Tags to aid search a categorization of course content
        public let tags: [String]
        /// The estimated amount of study hours necessary (in total)
        public let hours: Int
        /// The url for the image of this course
        public let image: String?
        /// The url for the course webpage
        public let url: String?
        
        public init(id: String, name: String, description: String, provider: String, contactTeaching: Bool, language: [String], location: String, model: String, schedule: [ScheduleSerializedState], tags: [String], hours: Int, image: String?, url: String?){
            
            self.id = id
            self.title = name
            self.description = description
            self.provider = provider
            self.contactTeaching = contactTeaching
            self.language = language
            self.location = location
            self.model = model
            self.schedule = schedule
            self.tags = tags
            self.hours = hours
            self.image = image
            self.url = url
            
        }
        
        public init?(coder aDecoder: NSCoder) {
            let id = aDecoder.decodeObject(forKey: "id") as! String
            let name = aDecoder.decodeObject(forKey: "title") as! String
            let description = aDecoder.decodeObject(forKey: "description") as! String
            let provider = aDecoder.decodeObject(forKey: "provider") as! String
            let contactTeaching = aDecoder.decodeObject(forKey: "contactTeaching") as! Bool
            let location = aDecoder.decodeObject(forKey: "location") as! String
            let language = aDecoder.decodeObject(forKey: "language") as! [String]
            let model = aDecoder.decodeObject(forKey: "model") as! String
            let tags = aDecoder.decodeObject(forKey: "tags") as! [String]
            let hours = aDecoder.decodeObject(forKey: "hours") as! Int
            let image = aDecoder.decodeObject(forKey: "image") as? String
            let url = aDecoder.decodeObject(forKey: "url") as? String
            let schedule = aDecoder.decodeObject(forKey: "schedule") as! [ScheduleSerializedState]
            
            
            self.init(id: id, name: name, description: description, provider: provider, contactTeaching: contactTeaching, language: language, location: location, model: model, schedule: schedule, tags: tags, hours: hours, image: image, url: url)
        }
    }
    
    /// The json parsed representation of a schedule
    public struct ScheduleSerializedState: Decodable {
        public let startWeek: Int
        public let endWeek: Int
        public let monday: [String]?
        public let tuesday: [String]?
        public let wednesday: [String]?
        public let thursday: [String]?
        public let friday: [String]?
        public let saturday: [String]?
        public let sunday: [String]?
        public let workDay: [String]?
        
        public init(startWeek: Int, endWeek: Int, monday: [String]?, tuesday: [String]?, wednesday: [String]?, thursday: [String]?, friday: [String]?, saturday: [String]?, sunday: [String]?, workDay: [String]?){
            
            self.startWeek = startWeek
            self.endWeek = endWeek
            self.monday = monday
            self.tuesday = tuesday
            self.wednesday = wednesday
            self.thursday = thursday
            self.friday = friday
            self.saturday = saturday
            self.sunday = sunday
            self.workDay = workDay
            
        }
        
        public init?(coder aDecoder: NSCoder) {
            let startWeek = aDecoder.decodeObject(forKey: "startWeek") as! Int
            let endWeek = aDecoder.decodeObject(forKey: "endWeek") as! Int
            let monday = aDecoder.decodeObject(forKey: "monday") as? [String]
            let tuesday = aDecoder.decodeObject(forKey: "tuesday") as? [String]
            let wednesday = aDecoder.decodeObject(forKey: "wednesday") as? [String]
            let thursday = aDecoder.decodeObject(forKey: "thursday") as? [String]
            let friday = aDecoder.decodeObject(forKey: "friday") as? [String]
            let saturday = aDecoder.decodeObject(forKey: "saturday") as? [String]
            let sunday = aDecoder.decodeObject(forKey: "sunday") as? [String]
            let workDay = aDecoder.decodeObject(forKey: "workDay") as? [String]
            
            
            
            self.init(startWeek: startWeek, endWeek: endWeek, monday: monday, tuesday: tuesday, wednesday: wednesday, thursday: thursday, friday: friday, saturday: saturday, sunday: sunday, workDay: workDay)
        }
        
    }
    
    /// The json parsed representation of a course model
    public struct CourseModelSerializedState: Decodable {
        public let mID: String
        public let name: String
        public let topics: [String]
        public let prerequisites: [String]
        public let level: Int
        
        public init(id: String, name: String, topics: [String], prerequisites: [String], level: Int){
            self.mID = id
            self.name = name
            self.topics = topics
            self.prerequisites = prerequisites
            self.level = level
        }
        
        public init?(coder aDecoder: NSCoder) {
            let id = aDecoder.decodeObject(forKey: "mID") as! String
            let name = aDecoder.decodeObject(forKey: "name") as! String
            let topics = aDecoder.decodeObject(forKey: "topics") as! [String]
            let prerequisites = aDecoder.decodeObject(forKey: "prerequisites") as! [String]
            let level = aDecoder.decodeObject(forKey: "level") as! Int
            
            self.init(id: id, name: name, topics: topics, prerequisites: prerequisites, level: level)
        }
    }
    
    /// The json parsed representation of a model relevance rationships one to many
    public struct RelevanceSerializedState: Decodable {
        public let completed: String
        public let targets: [String: Double]
        
        public init(completed: String, targets: [String: Double]){
            self.completed = completed
            self.targets = targets
        }
        
        public init?(coder aDecoder: NSCoder) {
            let completed = aDecoder.decodeObject(forKey: "completed") as! String
            let targets = aDecoder.decodeObject(forKey: "targets") as! [String: Double]
            
            self.init(completed: completed, targets: targets)
        }
    }
}

