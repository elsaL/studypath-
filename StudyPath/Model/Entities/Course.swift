import Foundation

/// Represents a concrete course offered by an institution or a website
public struct Course : Equatable {
    
    /// The name of the course
    public let name: String
    /// The description of the course
    public let description: String
    /// The provider of the course (istitution or website)
    public let provider: String
    /// Does the course have contact lessons?
    public let contactTeaching: Bool
    /// The language or languages of education for this course
    public let language: [String]
    /// The city where the course is held or available (blanck for online courses)
    public let location: String
    /// The model course this course is linked to
    public let model: ModelCourse
    /// The course reserved schedule
    public let schedule: ReservedSchedule
    /// Tags to aid search a categorization of course content
    public let tags: [String]
    /// The estimated amount of study hours necessary (in total)
    public let hours: Int
    /// The url for the image of this course
    public let imageUrl: String?
    /// The url for the course webpage
    public let url: String?
    
    /**
     * Create a course object form its components.
     * - Parameter name: the name of the course
     * - Parameter description: the description of the course
     * - Parameter provider: the provider of the course (istitution or website)
     * - Parameter model: the model course this course is linked to
     * - Parameter schedule: the course reserved schedule
     * - Parameter tags: tags to aid search a categorization of course content
     * - Parameter hours: the estimated amount of study hours necessary (in total)
     * - Parameter url: an optional url for the course webpage
     */
    public init(name: String, description: String, provider: String, contactTeaching: Bool, language: [String], location: String, model: ModelCourse, schedule: ReservedSchedule, tags: [String], hours: Int, imageUrl: String?, url: String?){
        
        self.name = name
        self.description = description
        self.provider = provider
        self.contactTeaching = contactTeaching
        self.language = language
        self.location = location
        self.model = model
        self.schedule = schedule
        self.tags = tags
        self.hours = hours
        self.imageUrl = imageUrl
        self.url = url
        
    }
    
    /**
     * Two courses are the same if they have the same name,
     * description and they are offered by the same provider
     */
    public static func == (lhs: Course, rhs: Course) -> Bool {
        return lhs.name == rhs.name &&
        lhs.description == rhs.description &&
        lhs.provider == rhs.provider
    }
}
