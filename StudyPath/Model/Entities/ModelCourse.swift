import Foundation

/**
 * Represents an abstract model for a type of course (e.g. "Introduction to programming")
 */
public class ModelCourse: Equatable {
    
    /**
     * A unique id for the model course
     */
    public let id: String
    
    /**
     * The name of the course
     */
    public let name: String
    
    /**
     * General topic this course belongs to
     */
    public let topics: [String]
    
    /**
     * Level of this model course (0 to 3)
     */
    public let level: Int
    
    /**
     * Create a ModelCourse form its component fields
     */
    init(id: String, name: String, level: Int, topics: [String]) {
        self.id = id
        self.name = name
        self.level = min(max(level, 0), 3)
        self.topics = topics
    }
    
    /**
     * Compare two ModelCourse by their id
     */
    public static func == (lhs: ModelCourse, rhs: ModelCourse) -> Bool {
        return lhs.id == rhs.id
    }
}
