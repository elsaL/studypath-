//
//  UserPreferences.swift
//  StudyPath
//
//  Created by iosdev on 05/05/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import Foundation

/**
 * Store the study path preferences of a user.
 */
public class UserPreferences: NSObject, NSCoding {
    
    /// The username of this preferences
    public let username: String
    /// The city the user lives in or wants to study in
    public let location: String
    /// The course delivery mode
    public let deliveryMode: String
    /// The schedule the user is available to study in
    public let schedulePreference: [String]
    /// Do not lower the score of courses outside of the user city
    public let allowFarAway: String
    /// Do not lower the score of paid courses
    public let allowPaid: String
    /// The amount of hours the user wants to study for in this study patj
    public let plannedStudyHours: String
    /// The rating for the topics
    public let topics: [String: TopicRating]
    /// The rating for the model courses
    public let models: [String: Int]
    /// The rating for the tags
    public let tags: [String]
    
    /**
     Create a user preference object
     */
    init(username: String, location:String, deliveryMode:String, schedulePreference:[String], allowFarAway:Bool, allowPaid: Bool, plannedStudyHours: Int, topics: [String: TopicRating], models: [String: Int], tags: [String]) {
        self.username = username
        self.location = location
        self.deliveryMode = deliveryMode
        self.schedulePreference = schedulePreference
        self.allowFarAway = allowFarAway ? "yes" : "no"
        self.allowPaid = allowPaid ? "yes" : "no"
        self.plannedStudyHours = String(plannedStudyHours)
        self.topics = topics
        self.models = models
        self.tags = tags
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(username, forKey: "username")
        aCoder.encode(location, forKey: "location")
        aCoder.encode(deliveryMode, forKey: "deliveryMode")
        aCoder.encode(schedulePreference, forKey: "schedulePreference")
        aCoder.encode(allowFarAway, forKey: "allowFarAway")
        aCoder.encode(allowPaid, forKey: "allowPaid")
        aCoder.encode(plannedStudyHours, forKey: "plannedStudyHours")
        aCoder.encode(String(decoding: try! JSONEncoder().encode(topics), as: UTF8.self), forKey: "topics")
        aCoder.encode(String(decoding: try! JSONEncoder().encode(models), as: UTF8.self), forKey: "models")
        aCoder.encode(String(decoding: try! JSONEncoder().encode(tags), as: UTF8.self), forKey: "tags")
    }
    
    public required convenience init?(coder aDecoder: NSCoder) {
        let username = aDecoder.decodeObject(forKey: "username") as! String
        let location = aDecoder.decodeObject(forKey: "location") as! String
        let deliveryMode = aDecoder.decodeObject(forKey: "deliveryMode") as! String
        let schedulePreference = aDecoder.decodeObject(forKey: "schedulePreference") as! [String]
        let allowFarAway = aDecoder.decodeObject(forKey: "allowFarAway") as! String
        let allowPaid = aDecoder.decodeObject(forKey: "allowPaid") as! String
        let plannedStudyHours = aDecoder.decodeObject(forKey: "plannedStudyHours") as! String
        
        let topicsJSON = aDecoder.decodeObject(forKey: "topics") as! String
        let modelJSON = aDecoder.decodeObject(forKey: "models") as! String
        let tagsJSON = aDecoder.decodeObject(forKey: "tags") as! String
        
        let topics = try! JSONDecoder().decode([String: TopicRating].self, from: topicsJSON.data(using: .utf8)!)
        let models = try! JSONDecoder().decode([String: Int].self, from: modelJSON.data(using: .utf8)!)
        let tags = try! JSONDecoder().decode([String].self, from: tagsJSON.data(using: .utf8)!)
        
        
        self.init(username: username, location: location, deliveryMode: deliveryMode, schedulePreference: schedulePreference, allowFarAway: allowFarAway == "yes", allowPaid: allowPaid == "yes", plannedStudyHours: Int(plannedStudyHours) ?? 0, topics: topics, models: models, tags: tags)
        
    }
}

/**
 * The user rating of a topic.
 */
public struct TopicRating: Codable {
    /// How much prior experience of the topic the user has
    public let experience: Double
    /// How much does the user like the topic
    public let preference: Double
    
    public init(experience: Double, preference: Double) {
        self.experience = experience
        self.preference = preference
    }
}
