//
//  StudyPathSerializedState.swift
//  StudyPath
//
//  Created by iosdev on 06/05/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import Foundation

/// The json parsed representation of a study path
public class StudyPathSerializedState: NSObject, NSCoding {
    public let items: [StudyPathItemSerializedState]
    
    public init(items: [StudyPathItemSerializedState]){
        self.items = items
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(items, forKey: "items")
    }
    
    public required convenience init?(coder aDecoder: NSCoder) {
        let items = aDecoder.decodeObject(forKey: "items") as! [StudyPathItemSerializedState]
        
        self.init(items: items)
    }
}

/// The json parsed representation of a study path item (course)
public class StudyPathItemSerializedState: NSObject, NSCoding {
    public let courseName: String
    public let courseDescription: String
    public let courseProvider: String
    public let status: String
    
    public init (courseName: String, courseDescription: String, courseProvider: String, status: String) {
        self.courseName = courseName
        self.courseDescription = courseDescription
        self.courseProvider = courseProvider
        self.status = status
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(courseName, forKey: "courseName")
        aCoder.encode(courseDescription, forKey: "courseDescription")
        aCoder.encode(courseProvider, forKey: "courseProvider")
        aCoder.encode(status, forKey: "status")
    }
    
    public required convenience init?(coder aDecoder: NSCoder) {
        let courseName = aDecoder.decodeObject(forKey: "courseName") as! String
        let courseDescription = aDecoder.decodeObject(forKey: "courseDescription") as! String
        let courseProvider = aDecoder.decodeObject(forKey: "courseProvider") as! String
        let status = aDecoder.decodeObject(forKey: "status") as! String
        
        self.init(courseName: courseName, courseDescription: courseDescription, courseProvider: courseProvider, status: status)
    }
}
