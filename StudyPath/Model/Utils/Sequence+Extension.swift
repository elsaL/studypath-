//
//  Array+Extension.swift
//  StudyPath
//
//  Created by iosdev on 23/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import Foundation

/// Extends Sequence to support zipWith
extension Sequence {
    /// Given two sequences a[i] and b[j] and a binary function f(x, y), return a single array [f(a[i], b[j])]
    func zipWith<S: Sequence, T>(_ that: S, combine: (Self.Element, S.Element) -> T) -> [T] {
        return zip(self, that).map{(a, b) in combine(a, b)}
    }
}
