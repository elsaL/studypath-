//
//  Strings.swift
//  StudyPath
//
//  Created by iosdev on 08/05/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import Foundation

/// Contains static references to localized strings to be used in the application
public class Strings {
    public static let start = NSLocalizedString("Start", comment: "")
    public static let next = NSLocalizedString("Next", comment: "")
    public static let back = NSLocalizedString("Back", comment: "")
    public static let cancel = NSLocalizedString("Cancel", comment: "")
    public static let done = NSLocalizedString("Done", comment: "")
    public static let alert = NSLocalizedString("Alert", comment: "Alert text")
    public static let ok = NSLocalizedString("OK", comment: "OK text")
    
    /// The localized strings of the Login View
    public class Login {
        public static let login = NSLocalizedString("Login", comment: "The login button text")
        public static let join = NSLocalizedString("Join Free", comment: "The join free button text")
        public static let username = NSLocalizedString("Username", comment: "Username word")
        public static let password = NSLocalizedString("Password", comment: "Password word")
        public static let tosA = NSLocalizedString("By creating an account, I accept LEARNIT’s", comment: "Terms of Service warning")
        public static let tosB = NSLocalizedString("Terms of Service", comment: "Terms of Service warning")
        public static let userRegistered = NSLocalizedString("User registered", comment: "Terms of Service warning")
        public static let usernameLengthError = NSLocalizedString("Username needs to be more than 2 characters", comment: "Username length error")
        public static let usernameExistsError = NSLocalizedString("Username exists already", comment: "Username exists error")
        public static let emailFormatIncorrect = NSLocalizedString("Email format is not correct", comment: "Incorrect email")
        public static let passwordFormatError = NSLocalizedString("Password needs to be more than 2 characters", comment: "Wrong password format")
        public static let emailEmptyError = NSLocalizedString("Email cannot be empty", comment: "Email cannot be empty text")
        
        public static let usernameDoesNotExistError = NSLocalizedString("Username does not exist", comment: "Username does not exist text")
        
        public static let passwordIncorrectError = NSLocalizedString("password is not correct", comment: "password not matching error text")
    }
    
    /// The localized strings of the Home View
    public class Home {
        public static let searchCourses = NSLocalizedString("Search Courses", comment: "Search Courses");
        
        public static let createStudyPath = NSLocalizedString("Construct Study Path", comment: "");
        
        public static let currentStudyPath = NSLocalizedString("Current Study Path", comment: "");
        
        public static let redoOnboarding = NSLocalizedString("Repeat Onboarding", comment: "");
        
        public static let logout = NSLocalizedString("Logout", comment: "");
        
    }
    
    /// The localized strings of the Onboarding View
    public class Onboarding {
        public static let learnITIntro = NSLocalizedString("LearnIT intro", comment: "");
        
        public static let welcome = NSLocalizedString("Welcome to LearnIT", comment: "");
        
        public static let questionTopics = NSLocalizedString("Which topics you are interested in?", comment: "");
        
        public static let experienceMessage = NSLocalizedString("Select your experience level", comment: "");
        
        public static let preferenceMessage = NSLocalizedString("How much do you prefer this topic?", comment: "");
        
        public static let modelLike = NSLocalizedString("How much do you like this model?", comment: "");
        
        public static let like = NSLocalizedString("Like", comment: "");
        
        public static let love = NSLocalizedString("Love", comment: "");
        
        public static let passion = NSLocalizedString("Passion", comment: "");
        
        public static let reset = NSLocalizedString("Reset", comment: "");
        
        public static let modelQuestion = NSLocalizedString("Select more preferred modules", comment: "");
        
        public static let selectTags = NSLocalizedString("Select tags", comment: "");
        
        public static let selectSomethingFirst = NSLocalizedString("Select something first", comment: "");
        
        public static let morning = NSLocalizedString("Morning", comment: "");
        
        public static let afternoon = NSLocalizedString("Afternoon", comment: "");
        
        public static let evening = NSLocalizedString("Evening", comment: "");
        
        public static let courseType = NSLocalizedString("Set courses type preference:", comment: "");
        
        public static let hoursPerDay = NSLocalizedString("Set the hours of studying per day", comment: "");
        
        public static let educationLength = NSLocalizedString("Set length of education (in months)", comment: "");
        
        public static let location = NSLocalizedString("Location", comment: "");
        
        public static let locationInput = NSLocalizedString("Input your location", comment: "");
        
        
        public static let oneMoreStep = NSLocalizedString("One more step", comment: "");
        
        public static let selectSchedulePreference = NSLocalizedString("Select schedule preference", comment: "")
        
        public static let paidPreference = NSLocalizedString("Are you fine with paid courses", comment: "")
        
        public static let distancePreference = NSLocalizedString("Are you fine with far away courses", comment: "")
        
        public static let fillAllField = NSLocalizedString("Please fill all fields", comment: "")
    }
    
}
