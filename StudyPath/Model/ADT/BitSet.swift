//
//  BitSet.swift
//  StudyPath
//
//  Created by iosdev on 22/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import Foundation

/// Represents a set implemented through bits and bit operations
struct BitSet<T: BinaryInteger>: ImmutableSet {
    
    /// The type of an element of the BitSet
    typealias Element = T

    /// The underlying BinaryInteger representation of the BitSet
    private let data: T
    
    /// Represents an empty BitSet
    static var empty: BitSet<T> {
        return BitSet(0)
    }
    
    /// The amount of elements in the set
    var count: Int {
        let possibleRange = 0..<data.bitWidth
        return possibleRange.filter{self.contains(integer: $0)}.count
    }
    
    private init (_ data: T){
        self.data = data
    }
    
    /**
     * Return a set containing one element
     * - Parameter value: the value in the set
     */
    static func singleton(_ value: T) throws -> BitSet<T> {
        if (value < 0 || value >= value.bitWidth) {
            throw BitSetError.valueOutOfRepresentableRange
        }
        return BitSet<T>(1 << value)
    }
    
    /**
     * Return a set containing the elements in the given array
     * - Parameter values: the values in the set
     */
    static func fromArray(_ values: [T]) throws -> BitSet<T> {
        let data: T = values.reduce(0, {result, value in result | (1 << value)})
        return BitSet<T>(data)
    }
    
    /**
     * Return true if an element is in the set
     * - Parameter member: the element
     */
    func contains(_ member: T) -> Bool {
        return self.contains(integer: Int(member))
    }
    
    /**
     * Return the union of this bitset and another
     * - Parameter other: the other bitset
     */
    func union(_ other: BitSet<T>) -> BitSet<T> {
        return BitSet(self.data | other.data)
    }
    
    /**
     * Return the intersection of this bitset and another
     * - Parameter other: the other bitset
     */
    func intersection(_ other: BitSet<T>) -> BitSet<T> {
        return BitSet(self.data & other.data)
    }
    
    /**
     * Return the difference of this bitset and another
     * - Parameter other: the other bitset
     */
    func difference(_ other: BitSet<T>) -> BitSet<T> {
        return BitSet(self.data & (~other.data))
    }
    
    /**
     * Return a normal set representetion of this bitset
     */
    func toIntSet() -> Set<Int> {
        let possibleRange = Set<Int>(Int(0)..<Int(data.bitWidth))
        return possibleRange.filter{self.contains(integer: $0)}
    }
    
    /// Check if the set contains an integer
    private func contains(integer: Int) -> Bool {
        return ((data >> integer) & 0x1) == 0x1
    }
    
    /// A BitSet error
    enum BitSetError: Error {
        case valueOutOfRepresentableRange
    }
    
}
