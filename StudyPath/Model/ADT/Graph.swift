//
//  Graph.swift
//  StudyPath
//
//  Created by Andrei Vasilev on 19/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import Foundation

/// Represents a directed graph holding data of type T in its node.
open class Graph<T>: Equatable {
    
    public typealias Element = T
    
    /// The graph represented as an adjacency dictionary
    public let adjacencyDict: [Vertex<Element>: [Vertex<Element>]]
    
    /// Represents the empty graph
    public static var empty: Graph<T> {
        return Graph([:])
    }
    
    /// The number of edges in the graph
    public var edgeCount: Int {
        return adjacencyDict.reduce(0, {result, entry in result + entry.value.count})
    }
    
    /// The number of vertices in the graph
    public var vertexCount: Int {
        return adjacencyDict.count
    }
    
    /// Private constructor to create a graph from an adjacency dictionary
    private init(_ adjacencyDict: [Vertex<T>: [Vertex<T>]]) {
        self.adjacencyDict = adjacencyDict
    }
    
    /// String representation of the graph
    public var description: CustomStringConvertible {
        var result = ""
        print(adjacencyDict)
        adjacencyDict.forEach{key, value in
            result.append("\(key) ---> [ \(value)] \n")
        }
        return result
    }
    
    /// All the vertices in this graph
    public var allVertices: [Vertex<Element>] {
        return self.adjacencyDict.map{$0.key}
    }
    
    /// Find vertices
    /// - Parameters:
    ///   - predicate: a predicate satisfied by the vertices to find
    /// - Returns: the vertices that contain the specified data
    public func findVertices(_ predicate: (Element) -> Bool) -> [Vertex<Element>]{
        return self.adjacencyDict.map{$0.key}.filter{predicate($0.data)}
    }
    
    /// Return the graph with an added vertex
    /// - Parameters:
    ///   - vertex: the new vertex
    ///   - edgesFrom: the nodes with edges pointing to the new node
    ///   - edgesTo: the nodes pointed by the edges of the new node
    /// - Returns: the updated graph
    public func withVertex(vertex: Vertex<Element>, edgesFrom: [Vertex<Element>], edgesTo: [Vertex<Element>]) -> Graph<Element> {
        // Add all the vertices that are in edgeFrom and edgesTo but not in the keys of adjancencyDict
        let dictWithAllFromVertices = edgesFrom.reduce(adjacencyDict, {result, from in
            let withUpdatedEdge: [Vertex<Element>: [Vertex<Element>]] = [from: []]
            return result.merging(withUpdatedEdge, uniquingKeysWith: {first, second in first})
        })
        let dictWithAllVertices = edgesTo.reduce(dictWithAllFromVertices, {result, to in
            let withUpdatedEdge: [Vertex<Element>: [Vertex<Element>]] = [to: []]
            return result.merging(withUpdatedEdge, uniquingKeysWith: {first, second in first})
        })
     
        // Create an adjacency dictionary with the edges ponting to the new vertex included
        let edgesFromDict = edgesFrom.reduce(dictWithAllVertices, {result, from in
            let withUpdatedEdge = adjacencyDict[from].map{[from: $0 + [vertex]]}
            return result.merging(withUpdatedEdge ?? [:], uniquingKeysWith: {first, second in second})
        })
        // Add the edges pointing away from the new vertex to complete the new adjacency dictionary
        let completeDict = edgesFromDict.merging([vertex: edgesTo], uniquingKeysWith: {first, second in second})
        // Return a new graph with the updated adjacency dictionary
        return Graph(completeDict)
    }
    
    /// Return the graph with an added vertex
    /// - Parameters:
    ///   - data: the data contained in the new vertex
    ///   - edgesFrom: the nodes with edges pointing to the new node
    ///   - edgesTo: the nodes pointed by the edges of the new node
    /// - Returns: the updated graph
    public func withVertex(data: Element, edgesFrom: [Vertex<Element>], edgesTo: [Vertex<Element>]) -> Graph<Element> {
        let vertex = Vertex(data: data)
        return self.withVertex(vertex: vertex, edgesFrom: edgesFrom, edgesTo: edgesTo)
    }
    
    /// Return the graph with an added vertex
    /// - Parameters:
    ///   - vertex: the new vertex
    /// - Returns: the updated graph
    public func withVertex(vertex: Vertex<Element>) -> Graph<Element> {
        return withVertex(vertex: vertex, edgesFrom: [], edgesTo: [])
    }
    
    /// Return the graph with an added vertex
    /// - Parameters:
    ///   - data: the data contained in the new vertex
    /// - Returns: the updated graph
    public func withVertex(data: Element) -> Graph<Element> {
        return withVertex(data: data, edgesFrom: [], edgesTo: [])
    }
    
    /// Return the graph with a vertex removed
    /// - Parameters:
    ///   - vertex: the vertex to be removed
    /// - Returns: the updated graph
    public func removedVertex(vertex: Vertex<Element>) -> Graph<Element> {
        var newAdjDict = adjacencyDict
        newAdjDict.removeValue(forKey: vertex)
        newAdjDict = newAdjDict.mapValues{$0.filter{v in v != vertex}}
        return Graph(newAdjDict)
    }
    
    /// Add directed edge between two vertices
    /// - Parameters:
    ///   - from: source vertex
    ///   - to: destination vertex
    /// - Returns: the updated graph
    public func withEdge(from: Vertex<Element>, to: Vertex<Element>) -> Graph<Element> {
        let dictWithNewVertices = adjacencyDict
            .merging([from: []], uniquingKeysWith: {fst, snd in fst})
            .merging([to: []], uniquingKeysWith: {fst, snd in fst})
        
        let withUpdatedEdge = dictWithNewVertices[from].map{[from: $0 + [to]]} ?? [:]
        return Graph(dictWithNewVertices.merging(withUpdatedEdge, uniquingKeysWith: {$1}))
    }
    
    /// Add the specified adjacency dictionary to the graph
    /// - Parameters:
    ///   - dictionary: the adjacency dictionary
    /// - Returns: the updated graph
    public func withAdjacencyDictionary(dictionary: [Vertex<Element>: [Vertex<Element>]]) -> Graph<Element> {
        return Graph(adjacencyDict.merging(dictionary, uniquingKeysWith: {$1}))
    }
    
    /// Find which vertices have edges pointing to the specified vertex
    /// - Parameters:
    ///   - vertex: the specified vertex
    /// - Returns: the vertices that have edges pointing to the specified vertex or empty array if the vertex is not in the graph
    public func incomingEdges(vertex: Vertex<Element>) -> [Vertex<Element>]{
        return self.adjacencyDict.filter{$0.value.contains(vertex)}.map{$0.key}
    }
    
    /// Find which vertices have edges pointing to them starting from the specified vertex
    /// - Parameters:
    ///   - vertex: the specified vertex
    /// - Returns: the vertices have edges pointing to them starting from the specified vertex or empty array if the vertex is not in the graph
    public func outgoingEdges(vertex: Vertex<Element>) -> [Vertex<Element>]{
        return self.adjacencyDict[vertex] ?? []
    }
    
    /// Return true if the graph contains the specified vertex
    /// - Parameters:
    ///   - vertex: the specified vertex
    /// - Returns: true if the graph contains the specified vertex
    public func contains(vertex: Vertex<Element>) -> Bool{
        return self.adjacencyDict[vertex] != nil
    }
    
    public static func == (lhs: Graph<T>, rhs: Graph<T>) -> Bool {
        return lhs.adjacencyDict == rhs.adjacencyDict
    }
    
    /**
     * Represents a vertex in a graph.
     */
    public class Vertex<T>: CustomStringConvertible, Hashable {
        
        /// The text representation of the vertex
        public var description: String {
            return "\(data)"
        }
        
        /// Contains the vertex data with generic type T
        let data: T
        
        /**
         * Create a vertex holding data
         * - Parameter data: the data
         */
        public init(data: T) {
            self.data = data
        }
        
        /// Reference equality between vertices
        public static func ==(lhs: Vertex, rhs: Vertex) -> Bool {
            return ObjectIdentifier(lhs) == ObjectIdentifier(rhs)
        }
        
        /// The hash value is obtained from the reference of the vertex
        public var hashValue: Int {
            return ObjectIdentifier(self).hashValue
        }
    }

}

/// An equatable Graph
extension Graph where Graph.Element: Equatable {
    /// Remove all the vertices that contain the specified data
    /// - Parameters:
    ///   - data: the data contained in the vertices to remove
    /// - Returns: the updated graph
    public func removedVertices(data: Element) -> Graph<Element>{
        let verticesToBeRemoved = findVertices(data: data)
        return verticesToBeRemoved.reduce(self, {result, vertex in result.removedVertex(vertex: vertex)})
    }
    
    /// Find vertices
    /// - Parameters:
    ///   - data: the data contained in the vertices to find
    /// - Returns: the vertices that contain the specified data
    public func findVertices(data: Element) -> Array<Vertex<Element>>{
        return self.findVertices{$0 == data}
    }
    
    /// Return true if the graph contains the specified data in any vertex
    /// - Parameters:
    ///   - data: the specified data
    /// - Returns: true if the graph contains the specified data in any of its vertices
    public func contains(data: Element) -> Bool{
        return self.adjacencyDict.contains(where: {key, value in key.data == data})
    }

}
