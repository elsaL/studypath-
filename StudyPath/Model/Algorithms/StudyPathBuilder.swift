//
//  StudyPathBuilder.swift
//  StudyPath
//
//  Created by iosdev on 07/05/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import Foundation

/**
 * Algorithm that builds a study path incrementally according to user preferences and
 * a dependency graph between the model courses.
 */
class StudyPathBuilder {
    /// How much having picked a model course will increase the score of other model courses
    private let relevances: [ModelRelevance]
    /// The user preferences from Onboarding
    private let userPreferences: UserPreferences?
    /// The dependency graph of the model courses
    private let modelGraph: Graph<ModelCourse>
    /// The courses rated with their scores, according to user preferences
    private let coursesWithScore: [CourseWithScore]
    /// The model courses rated with their scores, according to user preferences
    private let modelsWithScore: [ModelWithScore]
    
    /// Create a StudyPathBuilder from a database represented by a DataManager
    public convenience init (dataManager: DataManager){
        self.init (courses: dataManager.courses,
                   models: dataManager.models,
                   relevances: dataManager.relevanceRelationships,
                   userPreferences: dataManager.loggedIn.flatMap{user in dataManager.retrieveUserPreferences(username: user)},
                   modelGraph: dataManager.modelGraph,
                   pickedModels: [])
    }
    
    /// Create a StudyPathBuilder from its required data
    public init (courses: [Course], models: [ModelCourse], relevances: [ModelRelevance], userPreferences: UserPreferences?, modelGraph: Graph<ModelCourse>, pickedModels: [ModelCourse]){
        self.relevances = relevances
        self.userPreferences = userPreferences
        self.modelGraph = modelGraph
        self.modelsWithScore = StudyPathBuilder.computeModelScores(models: models, picked: pickedModels, relevances: relevances, userPreferences: userPreferences)
        self.coursesWithScore = StudyPathBuilder.computeCoursesScores(courses: courses, userPreferences: userPreferences)
        
    }
    
    /**
     * Return the courses the user could be picking at this level of path construction
     * - Parameter picked: the already picked courses
     * - Parameter maxCount: the maximum amount of courses to return
     */
    public func currentChoices(picked: [Course], maxCount: Int) -> [Course] {
        let pickedModels = getModelsFromCourses(courses: picked)
        return currentChoices(picked: pickedModels, maxCount: maxCount)
    }
    
    /**
     * Return the courses the user could be picking at this level of path construction
     * - Parameter picked: the models of the already picked courses
     * - Parameter maxCount: the maximum amount of courses to return
     */
    public func currentChoices(picked: [ModelCourse], maxCount: Int) -> [Course] {
        // Remove the already picked models from the graphs, to find the current starting points
        let graphWithoutPicked = picked.reduce(modelGraph, {$0.removedVertices(data: $1)})
        // Check models to exclude because too easy for the user
        let tooEasyModels = modelsWithScore.filter{$0.score < -0.6}.map{$0.course}
        // Remove the too easy models from the graph
        let graphWithoutTooEasy = tooEasyModels.reduce(graphWithoutPicked, {$0.removedVertices(data: $1)})
        // Find the current starting points (vertices with no incoming edges)
        let roots = graphWithoutTooEasy.allVertices
            .filter{graphWithoutTooEasy.incomingEdges(vertex: $0).count == 0}
        // Score the current starting points
        let rootsWithScores = roots.map{root -> (ModelCourse, Double) in
            // The base score of the starting point
            let staticScore = modelsWithScore.filter{$0.course == root.data}.first?.score ?? 0.0
            // The recursive score found by looking recursively at possible future choices
            let dynamicScore = computeBestScore(depth: 0, maxDepth: 30, graph: graphWithoutPicked, start: root)
            /*
             * The final score is made by one part current score and two parts possible future score.
             * The future score (best possible path) is divided by the steps in the path, to find an average.
             */
            return (root.data, staticScore + 2 * (dynamicScore.1 / Double(dynamicScore.0)))
        }
        // Sort the starting points by score
        let sortedRoots = rootsWithScores.sorted(by: {$0.1 > $1.1}).map{$0.0}
        
        // Proportions of results from each starting point, in tenths
        let proportions = [6, 3, 1]
        
        // Fetch the concrete courses from the sorted model courses, according to the specified proportions
        let results = sortedRoots.zipWith(proportions, combine: {model, proportion -> [Course] in
            let amount: Int = (proportion * maxCount) / 10
            return Array(getAllSortedCoursesFromModel(model: model).prefix(amount))
        }).flatMap{$0}
        
        return results
    }
    
    /**
     * Compute recursively the best score attainable by starting from the "start" node, up to a depth
     * of "maxDepth", in the specified graph.
     *
     * - Parameter depth: the current depth (start with 0)
     * - Parameter maxDepth: the max depth of the search
     * - Parameter graph: the specified graph
     * - Parameter start: the starting node of the search
     * - Returns: A pair of the length of the best path  and its total score
     */
    private func computeBestScore(depth: Int, maxDepth: Int, graph: Graph<ModelCourse>, start: Graph<ModelCourse>.Vertex<ModelCourse>) -> (Int, Double) {
        // All the edges that go outwards from the start
        let next = graph.outgoingEdges(vertex: start)
        
        // Get the score of the starting node
        let startNodeScore = modelsWithScore.filter{$0.course.id == start.data.id}.first?.score ?? 0.0
        
        // The ending conditions for the recursion
        if depth == maxDepth || next.count == 0 {
            return (depth, startNodeScore)
        }
        // The graph without the starting vertex
        let graphWithoutStart = graph.removedVertex(vertex: start)
        
        //Compute the max average store for all the paths from start
        let scores = next.map{
            computeBestScore(depth: depth + 1, maxDepth: maxDepth, graph: graphWithoutStart, start: $0)
        }
        // The maximum score attainable and the depth of the related path, in a pair
        let maxScoreWithDepth = scores.max(by: {$0.1 < $1.1})
        
        return (maxScoreWithDepth?.0 ?? maxDepth, startNodeScore + (maxScoreWithDepth?.1 ?? 0))
    }
    
    /// Get the courses sorted by their score and that share a model
    private func getAllSortedCoursesFromModel(model: ModelCourse) -> [Course]{
        return coursesWithScore.filter{$0.course.model == model}.map{$0.course}
    }
    
    /// Get all the models of a list of courses
    private func getModelsFromCourses(courses: [Course]) -> [ModelCourse]{
        return Set(courses.map{$0.model.id})
            .compactMap{id in modelsWithScore.filter{$0.course.id == id}.first?.course}
    }
    
    /**
     * Compute the base score of model courses, according to topic preference
     * and experience and previously picked model courses
     */
    private static func computeModelScores(models: [ModelCourse], picked: [ModelCourse], relevances: [ModelRelevance], userPreferences: UserPreferences?) -> [ModelWithScore]{
        let preferredTopics = userPreferences?.topics ?? [:]
        let preferredModels = userPreferences?.models ?? [:]
        
        return models.map{model in
            let topicScores = model.topics.reduce(0.0, {score, topic in
                let experienceTopic = preferredTopics[topic]?.experience ?? 1.0
                let preferenceTopic = preferredTopics[topic]?.preference ?? 1.0
                //Note: preferredModels comes from onboarding, and uses model.name as key instead if model.id
                let preferenceModel = Double(preferredModels[model.name] ?? 0)
                
                let fromPicked = relevances
                    .filter{$0.target.id == model.id}
                    .reduce(0.0, {result, mr in result + mr.score})
                
                // Penalty for low level with experience
                let modifier = (experienceTopic * 1.5) - Double(model.level)
                
                /**
                 * The score is proportional to the preference and the related picked models, while
                 * inversely proportional to the difference between user experience and course level
                 */
                return preferenceModel + (fromPicked / 20.0) + preferenceTopic - 3 * ((modifier > 0) ? modifier : 0)
            })
            
            return ModelWithScore(course: model, score: topicScores)
        }.sorted(by: {a, b in a.score > b.score})
    }
    
    /// Compute the base score of courses, given user preferences
    private static func computeCoursesScores(courses: [Course], userPreferences: UserPreferences?) -> [CourseWithScore]{
        let allowFarAway = userPreferences?.allowFarAway == "yes"
        let allowPaid = userPreferences?.allowPaid == "yes"
        let mode = (userPreferences?.deliveryMode ?? "Contact").lowercased()
        let location = userPreferences?.location ?? "Helsinki"
        let tags = userPreferences?.tags ?? []
        let preferredScheduleTimes = (userPreferences?.schedulePreference)?.compactMap{elem -> WeeklySchedule.TimeSlot? in
            if (elem.lowercased() == "morning"){
                return WeeklySchedule.TimeSlot.morning
            } else if (elem.lowercased() == "afternoon") {
                return WeeklySchedule.TimeSlot.afternoon
            } else if (elem.lowercased() == "evening"){
                return WeeklySchedule.TimeSlot.evening
            } else {
                return nil
            }
        }
        let times = (preferredScheduleTimes?.count ?? 0) > 0 ? preferredScheduleTimes : []
        let unavailableTime = ReservedSchedule.empty.withReserved(year: 0, weeks: Array(0..<53), weeklySchedule: WeeklySchedule.empty
            .withSameBookingsEveryWorkingDay(times: times ?? [.evening]).complement)
        
        return courses.map{course in
            // Remove 3 points if the course is far away and the user does not like that
            let scoreA = !allowFarAway && mode == "contact" && course.location != location ? -3.0 : 0.0
            // Remove 1 point if the course is far paid and the user does not like that
            let scoreB = !allowPaid && course.description.contains("price") ? -1.0 : 0.0
            // Grant 0.5 points if the course of the type the user prefers
            let scoreC =
                (mode == "contact" && course.contactTeaching) ||
                (mode != "contact" && !course.contactTeaching)
                    ? 0.5 : 0.0
            // Grant 0.5 points for each user selected tag the course contains
            let scoreD = Double(course.tags.filter{tags.contains($0)}.count) * 0.5
            // Remove -0.3 points for a possible clash
            let scoreE = course.schedule.maybeClashes(unavailableTime) ? -0.3 : 0.0
            // Remove -1.0 points for a sure clash (a sure clash is also a possible clash)
            let scoreF = course.schedule.surelyClashes(unavailableTime) ? -0.7 : 0.0
            // The sum of the partial scores computer above
            let score = scoreA + scoreB + scoreC + scoreD + scoreE + scoreF
            return CourseWithScore(course: course, score: score)
        }.sorted(by: {a, b in a.score > b.score})
    }
    
    /// A course and its score, according to user perferences
    public struct CourseWithScore {
        let course: Course
        let score: Double
    }
    
    /// A model course and its score, according to user perferences
    public struct ModelWithScore {
        let course: ModelCourse
        let score: Double
    }
}
