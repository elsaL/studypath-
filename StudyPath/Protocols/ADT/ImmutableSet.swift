//
//  ImmutableSet.swift
//  StudyPath
//
//  Created by iosdev on 22/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import Foundation

/// Represents a set that cannot be changed
protocol ImmutableSet: Equatable {
    associatedtype Element: BinaryInteger
    
    /// The amount of elements in the set
    var count: Int { get }

    /**
     * Return true if this set contains the given element
     * - Parameter member: the given element
     */
    func contains(_ member: Element) -> Bool
    
    /**
     * Return the union of this set and another
     * - Parameter other: the other set
     */
    func union(_ other: Self) -> Self
    
    /**
     * Return the intersection of this set and another
     * - Parameter other: the other set
     */
    func intersection(_ other: Self) -> Self
    
    /**
     * Return the difference of this set and another
     * - Parameter other: the other set
     */
    func difference(_ other: Self) -> Self
    
}

extension ImmutableSet {
    
    /**
     * Return the union of two sets
     * - Parameter this: the first set
     * - Parameter that: the second set
     */
    static func + (this: Self, that: Self) -> Self {
        return this.union(that)
    }
    
    /**
     * Return the difference of two sets
     * - Parameter this: the first set
     * - Parameter that: the second set
     */
    static func - (this: Self, that: Self) -> Self {
        return this.difference(that)
    }
    
    /**
     * Return the symmetric difference of this set and another
     * - Parameter other: the other set
     */
    func symmetricDifference(_ other: Self) -> Self {
        return (self - other) + (other - self)
    }
}
